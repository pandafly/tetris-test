<?php

use ThemeOptions\RegisterMenus;

$registerMainHeader = new RegisterMenus('MainHeader', 'tetris');


?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/dest/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/dest/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/dest/favicons/favicon-16x16.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="loader">
    <div class="loader__img-wrapper">
        <?php if (function_exists('the_custom_logo') && has_custom_logo()) the_custom_logo(); ?>
    </div>
</div>
<?php
get_template_part('template-parts/header/main', 'header', ['menu' => $registerMainHeader]);
?>
