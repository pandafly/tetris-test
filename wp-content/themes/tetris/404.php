<?php
/*
Template Name: 404
*/
get_header();
?>

    <main class="error-page scroller">
        <div class="container-fluid-min max-width">
            <section class="error-404 not-found">
                <div class="page-content">
                    <h1><?php esc_html_e('404 Not Found', 'tetris') ?></h1>
                    <p><?php esc_html_e('It looks like nothing was found at this location.', 'tetris'); ?></p>
                </div>
            </section>
        </div>
    </main>

<?php
get_footer();
