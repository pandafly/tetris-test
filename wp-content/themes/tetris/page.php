<?php
get_header();
?>

    <main class="page-default scroller">
        <?php
        while (have_rows('flexible', get_the_ID())) {
            the_row();
            get_template_part('templates/blocks/' . get_row_layout());
        }
        ?>
    </main>

<?php
get_footer();
