<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$options = get_fields('options');
$changeColors = CustomFunctions::changeColor($options['footer']['colors']);
$adminStyle = CustomFunctions::styleControl($options['footer']);
?>
<footer id="colophon" class="site-footer <?php echo $changeColors; ?>" <?php echo $adminStyle ?>>
    <div class="max-width-full container-fluid-min">
        <div class="site-footer__wrapper">
            <div class="site-footer__wrapper__logo">
                <a href="/">
                    <?php if ($options['footer']['logo_svg']): ?>
                        <?php echo file_get_contents($options['footer']['logo_svg']) ?>
                    <?php else:
                        echo wp_get_attachment_image($options['footer']['logo']);
                    endif; ?>
                </a>
            </div>
            <div class="site-footer__wrapper__col <?php echo Helpers::get($options, 'footer.hide_on_mobile') ? 'hide_mob' : '' ?>">
                <?php if (Helpers::get($options, 'footer.content')): ?>
                    <?php foreach (Helpers::get($options, 'footer.content') as $item):
                        echo get_template_part('templates/parts/footer', 'row', $item);
                    endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="site-footer__wrapper__col <?php echo Helpers::get($options, 'footer.hide_on_mobile_second') ? 'hide_mob' : '' ?>">
                <?php if (Helpers::get($options, 'footer.content_second')): ?>
                    <?php foreach (Helpers::get($options, 'footer.content_second') as $item):
                        echo get_template_part('templates/parts/footer', 'row', $item);
                    endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="site-footer__wrapper__col <?php echo Helpers::get($options, 'footer.hide_on_mobile_third') ? 'hide_mob' : '' ?>">
                <?php if (Helpers::get($options, 'footer.content_third')): ?>
                    <?php foreach (Helpers::get($options, 'footer.content_third') as $item):
                        echo get_template_part('templates/parts/footer', 'row', $item);
                    endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="site-footer__wrapper__bottom">
                <?php if (Helpers::get($options, 'footer.bottom_links')): ?>
                    <?php foreach (Helpers::get($options, 'footer.bottom_links') as $item): ?>
                        <?php if ($item['type'] == 0): ?>
                            <p class="site-footer__wrapper__bottom__text">
                                <?php echo $item['text'] ?? '' ?>
                            </p>
                        <?php else: ?>
                            <a class="site-footer__wrapper__bottom__link"
                               target="<?php echo $item['link']['target'] ?: '_self' ?>"
                               href="<?php echo $item['link']['url'] ?? '' ?>"><?php echo $item['link']['title'] ?? '' ?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
