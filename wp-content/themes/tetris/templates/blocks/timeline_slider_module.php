<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$years = [];
if (Helpers::get($subFields, 'content.items')) {
    foreach (Helpers::get($subFields, 'content.items') as $item) {
        $years[] = $item['year'];
    }
}
?>

<section class="timeline <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="timeline__wrapper">
            <div class="timeline__wrapper__head">
                <h2><?php echo Helpers::get($subFields, 'content.title'); ?></h2>
                <?php if (count(Helpers::get($subFields, 'content.items')) > 1) : ?>
                    <div class="swiper_nav">
                        <button class="timeline__prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M22 13.5C22.8284 13.5 23.5 12.8284 23.5 12C23.5 11.1716 22.8284 10.5 22 10.5V13.5ZM0.939341 10.9393C0.353554 11.5251 0.353554 12.4749 0.939341 13.0607L10.4853 22.6066C11.0711 23.1924 12.0208 23.1924 12.6066 22.6066C13.1924 22.0208 13.1924 21.0711 12.6066 20.4853L4.12132 12L12.6066 3.51472C13.1924 2.92893 13.1924 1.97919 12.6066 1.3934C12.0208 0.807611 11.0711 0.807611 10.4853 1.3934L0.939341 10.9393ZM22 10.5L2 10.5V13.5L22 13.5V10.5Z"
                                      fill="white"/>
                            </svg>
                        </button>
                        <button class="timeline__next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M2 10.5C1.17157 10.5 0.5 11.1716 0.5 12C0.5 12.8284 1.17157 13.5 2 13.5L2 10.5ZM23.0607 13.0607C23.6464 12.4749 23.6464 11.5251 23.0607 10.9393L13.5147 1.3934C12.9289 0.807612 11.9792 0.807612 11.3934 1.3934C10.8076 1.97919 10.8076 2.92893 11.3934 3.51472L19.8787 12L11.3934 20.4853C10.8076 21.0711 10.8076 22.0208 11.3934 22.6066C11.9792 23.1924 12.9289 23.1924 13.5147 22.6066L23.0607 13.0607ZM2 13.5L22 13.5L22 10.5L2 10.5L2 13.5Z"
                                      fill="white"/>
                            </svg>
                        </button>
                    </div>
                <?php endif; ?>
            </div>
            <div class="timeline__wrapper__slider timeline-slider" data-years="<?php echo implode(',', $years); ?>">
                <div class="swiper-wrapper">
                    <?php if (Helpers::get($subFields, 'content.items')): ?>
                        <?php foreach (Helpers::get($subFields, 'content.items') as $item): ?>
                            <div class="swiper-slide">
                                <div class="timeline-slider__single">
                                    <?php echo wp_get_attachment_image($item['image'], 'full') ?>
                                    <div class="timeline-slider__single__content">
                                        <h2><?php echo $item['year'] ?? '' ?></h2>
                                        <div class="timeline-slider__single__content__text">
                                            <?php echo $item['description'] ?? '' ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <?php if (count(Helpers::get($subFields, 'content.items')) > 1): ?>
                    <div class="timeline__pagina"></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>