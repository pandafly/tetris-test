<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$type = Helpers::get($subFields, 'content.type') == 0 ? '' : 'title_descr';
$underBg = 'background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)),url(' . wp_get_attachment_image_url(Helpers::get($subFields,
        "content.second_layer.image"),
        "full") . ') 50% 100% / cover no-repeat';
?>

<section class="hero-module <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="hero-module__wrapper <?php echo $type; ?>">
            <div class="hero-module__layer-under__content"
                 style="<?php echo Helpers::get($subFields,
                     "content.second_layer.image") ? $underBg : ''; ?>">
                <div class="hero-module__layer-under__inner">
                    <h1>
                        <?php if (Helpers::get($subFields, 'content.second_layer.title')): ?>
                            <?php foreach (Helpers::get($subFields, 'content.second_layer.title') as $item): ?>
                                <span><?php echo $item['row']; ?></span>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </h1>
                    <?php if ($type): ?>
                        <div class="hero-module__wrapper__descr">
                            <?php echo Helpers::get($subFields, 'content.second_layer.description'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="hero-module__layer-above">
                <div class="hero-module__layer-above__image"
                     style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)),url('<?php echo wp_get_attachment_image_url(Helpers::get($subFields,
                         'content.first_layer.image'),
                         'full') ?>') 50% 100% / cover no-repeat"></div>
                <div class="hero-module__layer-above__content">
                    <div class="hero-module__layer-above__inner">
                        <h1>
                            <?php if (Helpers::get($subFields, 'content.first_layer.title')): ?>
                                <?php foreach (Helpers::get($subFields, 'content.first_layer.title') as $item): ?>
                                    <span><?php echo $item['row']; ?></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </h1>
                        <?php if ($type): ?>
                            <div class="hero-module__wrapper__descr">
                                <?php echo Helpers::get($subFields, 'content.first_layer.description'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
