<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
	'content',
	'colors',
	'settings',
];
$subFields    = CustomFunctions::getSubFields( $acfFieldKeys );
$bgAdd        = CustomFunctions::styleControl( $subFields['settings'] );
$changeColors = CustomFunctions::changeColor( $subFields['colors'] );
$scroll       = count( $subFields['content']['slides'] ) > 1 ? 'scroll' : '';
$arrSlides    = $subFields['content']['slides'];
?>

<section id="vertical_slider2" class="v-slider2 <?php echo $scroll . ' ' . $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full">
        <div class="v-slider2__outer-wrapper">
            <div class="v-slider2__label"><?= $subFields['content']['label'] ?></div>
        </div>
        <div class="v-slider2__wrapper"
             style="background-color:rgb(<?= $arrSlides[0]['background_color_radio'] ?>);">

		    <?php if ( $subFields['content']['slides'] ):
			    foreach ( $arrSlides as $key => $item ):
				    $bgColor = $item['background_color_radio'] !== 'default' ? $item['background_color_radio'] : '';
				    ?>
                    <div class="v-slider2__slide" data-bgcolor="<?= $bgColor ?>"
                         style="z-index: <?= $key > 0 ? - $key : 1 ?>;background-color:rgb(<?= $bgColor ?>);">
                        <div class="v-slider2__slide__content">
                            <div>
							    <?= $item['text'] ?>
							    <?php if ( $item['button_enable'] ): ?>
                                    <a class="v2-slider__content__inner__link changable"
                                       target="<?php echo Helpers::get( $item, 'button.target' ) ?: '_self'; ?>"
                                       href="<?php echo Helpers::get( $item, 'button.url' ) ?>">
									    <?php echo Helpers::get( $item, 'button.title' ) ?>
                                    </a>
							    <?php endif; ?>
                            </div>
                        </div>
                        <div class="v-slider2__image__wrapper">
                            <img src="<?php echo $item['image'] ?>">
                        </div>
                    </div>
			    <?php endforeach; ?>
		    <?php endif; ?>
        </div>
    </div>
</section>