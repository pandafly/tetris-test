<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="single-images <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-min container-fluid-min">
        <div class="single-images__wrapper">
            <?php if (Helpers::get($subFields, 'content.repeater')): ?>
                <div class="single-images__wrapper__loop">
                    <?php foreach (Helpers::get($subFields, 'content.repeater') as $item):
                        if ($item['type'] == 0):?>
                            <div class="single-images__wrapper__single">
                                <?php echo wp_get_attachment_image($item['image'], 'full') ?>
                                <?php if ($item['caption']): ?>
                                    <div class="single-images__wrapper__caption">
                                        <?php echo $item['caption'] ?? '' ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php else:
                            if ($item['images']): ?>
                                <div class="single-images__wrapper__multiple">
                                    <?php foreach ($item['images'] as $img): ?>
                                        <div class="single-images__wrapper__multiple__img">
                                            <?php echo wp_get_attachment_image($img['image'], 'full') ?>
                                            <?php if ($img['caption']): ?>
                                                <div class="single-images__wrapper__caption">
                                                    <?php echo $img['caption'] ?? '' ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif;
                        endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
