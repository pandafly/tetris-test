<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="text-slider <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="text-slider__wrapper">
            <?php if (Helpers::get($subFields, 'content.row')): ?>
                <div class="text-slider__loop">
                    <?php foreach (Helpers::get($subFields, 'content.row') as $item): ?>
                        <?php if ($item['words']): ?>
                            <div class="text-slider__loop__row" data-direction="<?php echo $item['direction'] == 1 ? 'rtl' : ''; ?>">
                                <?php foreach ($item['words'] as $word): ?>
                                    <h3>
                                        <?php echo $word['text'] ? $word['text'] . ' – ' : '' ?>
                                    </h3>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>