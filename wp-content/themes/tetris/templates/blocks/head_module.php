<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="head <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="head__wrapper">
            <h1><?php echo Helpers::get($subFields, 'content.title'); ?></h1>
            <?php if (Helpers::get($subFields, 'content.sub_pages.repeater')): ?>
                <div class="head__wrapper__sub-pages desktop">
                    <?php foreach (Helpers::get($subFields, 'content.sub_pages.repeater') as $item): ?>
                        <div class="head__wrapper__sub-pages__single <?php echo $item['current_page'] ? 'active' : '' ?>">
                            <a href="<?php echo $item['link'] ? get_the_permalink($item['link']) : '' ?>">
                                <?php echo $item['title'] ?? '' ?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="head__wrapper__sub-pages mobile head_slider">
                    <div class="swiper-wrapper">
                        <?php foreach (Helpers::get($subFields, 'content.sub_pages.repeater') as $key => $item): ?>
                            <div class="swiper-slide">
                                <div data-key="<?php echo $key ?>"
                                     class="head__wrapper__sub-pages__single <?php echo $item['current_page'] ? 'active' : '' ?>">
                                    <a href="<?php echo $item['link'] ? get_the_permalink($item['link']) : '' ?>">
                                        <?php echo $item['title'] ?? '' ?>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
