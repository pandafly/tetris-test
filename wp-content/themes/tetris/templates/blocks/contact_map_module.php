<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="contact-map <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="contact-map__wrapper">
            <div class="contact-map__wrapper__content">
                <div class="contact-map__head">
                    <h2><?php echo Helpers::get($subFields, 'content.left.title'); ?></h2>
                    <div class="contact-map__head__descr">
                        <?php echo Helpers::get($subFields, 'content.left.description'); ?>
                    </div>
                </div>
                <div class="contact-map__main">
                    <div class="contact-map__main__address">
                        <h4><?php echo Helpers::get($subFields, 'content.left.address.title') ?></h4>
                        <div class="contact-map__main__address__text">
                            <?php echo Helpers::get($subFields, 'content.left.address.text') ?>
                        </div>
                        <a target="<?php echo Helpers::get($subFields, 'content.left.address.link.target') ?: '_self'; ?>"
                           href="<?php echo Helpers::get($subFields, 'content.left.address.link.url') ?>">
                            <?php echo Helpers::get($subFields, 'content.left.address.link.title') ?>
                        </a>
                    </div>
                    <div class="contact-map__main__contacts">
                        <?php if (Helpers::get($subFields, 'content.left.contacts')): ?>
                            <?php foreach (Helpers::get($subFields, 'content.left.contacts') as $item): ?>
                                <div class="contact-map__main__contacts__single">
                                    <?php if ($item['title']): ?>
                                        <h4><?php echo $item['title'] ?></h4>
                                    <?php endif; ?>
                                    <?php if ($item['email']): ?>
                                        <a class="contact_email" href="mailto:<?php echo $item['email'] ?>"><?php echo $item['email'] ?></a>
                                    <?php endif; ?>
                                    <?php if ($item['tel']): ?>
                                        <a href="tel:<?php echo $item['tel'] ?>"><?php echo $item['tel'] ?></a>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="contact-map__wrapper__map">
                <div class="contact-map__popup">
                    <div class="contact-map__popup__inner__close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                            <path d="M10.0012 11.9335L3.60117 18.3335C3.26783 18.6887 2.86223 18.8663 2.38437 18.8663C1.9065 18.8663 1.5009 18.6887 1.16757 18.3335C0.812366 18.0002 0.634766 17.5946 0.634766 17.1167C0.634766 16.6389 0.812366 16.2333 1.16757 15.8999L7.56757 9.49994L1.16757 3.09994C0.812366 2.76661 0.634766 2.36101 0.634766 1.88315C0.634766 1.40528 0.812366 0.999678 1.16757 0.666345C1.5009 0.311145 1.9065 0.133545 2.38437 0.133545C2.86223 0.133545 3.26783 0.311145 3.60117 0.666345L10.0012 7.06634L16.4012 0.666345C16.7345 0.311145 17.1401 0.133545 17.618 0.133545C18.0958 0.133545 18.5014 0.311145 18.8348 0.666345C19.19 0.999678 19.3676 1.40528 19.3676 1.88315C19.3676 2.36101 19.19 2.76661 18.8348 3.09994L12.4348 9.49994L18.8348 15.8999C19.19 16.2333 19.3676 16.6389 19.3676 17.1167C19.3676 17.5946 19.19 18.0002 18.8348 18.3335C18.5014 18.6887 18.0958 18.8663 17.618 18.8663C17.1401 18.8663 16.7345 18.6887 16.4012 18.3335L10.0012 11.9335Z"
                                  fill="#1C1B1F"/>
                        </svg>
                    </div>
                    <div class="contact-map__popup__inner">
                        <h4><?php echo Helpers::get($subFields, 'content.right_part.popup.title') ?></h4>
                        <div class="contact-map__popup__inner__text">
                            <?php echo Helpers::get($subFields, 'content.right_part.popup.text') ?>
                        </div>
                        <a target="<?php echo Helpers::get($subFields, 'content.right_part.popup.link.target') ?: '_self'; ?>"
                           href="<?php echo Helpers::get($subFields, 'content.right_part.popup.link.url') ?>">
                            <?php echo Helpers::get($subFields, 'content.right_part.popup.link.title') ?>
                        </a>
                    </div>
                </div>
                <div class="contact-map__wrapper__map__inner">
                    <div id="map"
                         data-icon="<?php echo Helpers::get($subFields, 'content.right_part.map_icon') ?>"
                         data-icon-mob="<?php echo Helpers::get($subFields, 'content.right_part.map_icon_mobile') ?>"
                         data-lat="<?php echo Helpers::get($subFields, 'content.right_part.latitude') ?>"
                         data-lng="<?php echo Helpers::get($subFields, 'content.right_part.longitude') ?>"
                         class="map__wrapper">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
