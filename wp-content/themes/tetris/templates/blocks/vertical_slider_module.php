<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$reverse = Helpers::get($subFields, 'content.reverse') ? 'reverse' : '';
$scroll = count($subFields['content']['slides']) > 1 ? 'scroll' : '';
?>

<section id="vertical_slider" class="v-slider desktop <?php echo $scroll . ' ' . $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="v-slider__wrapper desktop <?php echo $reverse; ?>">
            <div class="v-slider__wrapper__images">
                <?php if ($subFields['content']['slides']):
                    $arrImgs = $subFields['content']['slides'];
                    $last_key = key(array_slice($arrImgs, -1, 1, true))
                    ?>
                    <div class="v-slider__images__wrap">
                        <div class="v-slider__images__list">
                            <?php foreach (array_reverse($arrImgs) as $key => $item): ?>
                                <div style="height: calc(100% - <?php echo $key * 50 . 'px'; ?>)"
                                     class="v-slider__images__list__single <?php echo $key == $last_key ? 'active' : '' ?>">
                                    <img width="100%" height="100%"
                                         src="<?php echo $item['image'] ?>">
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="v-slider__wrapper__content">
                <?php if ($subFields['content']['slides']): ?>
                    <div class="v-slider__content__list">
                        <?php foreach ($subFields['content']['slides'] as $key => $item): ?>
                            <div class="v-slider__content__list__single <?php echo $key == 0 ? 'active' : ''; ?>">
                                <div class="v-slider__content__list__inner"><?php echo $item['text'] ?></div>
                                <?php if ($item['button_enable']): ?>
                                    <a class="v-slider__content__list__inner__link changable"
                                       target="<?php echo Helpers::get($item, 'button.target') ?: '_self'; ?>"
                                       href="<?php echo Helpers::get($item, 'button.url') ?>">
                                        <?php echo Helpers::get($item, 'button.title') ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section id="vertical_slider" class="v-slider scroll mobile <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="v-slider__wrapper mobile">
            <?php if ($subFields['content']['slides']): ?>
                <div class="v-slider__content__list">
                    <?php foreach ($subFields['content']['slides'] as $key => $item): ?>
                        <div class="v-slider__list__mobile">
                            <div class="v-slider__images__list__single">
                                <img width="100%" height="100%"
                                     src="<?php echo $item['image'] ?>">
                            </div>
                            <div class="v-slider__content__list__single">
                                <div class="v-slider__content__list__inner"><?php echo $item['text'] ?></div>
                                <?php if ($item['button_enable']): ?>
                                    <a class="v-slider__content__list__inner__link changable"
                                       target="<?php echo Helpers::get($item, 'button.target') ?: '_self'; ?>"
                                       href="<?php echo Helpers::get($item, 'button.url') ?>">
                                        <?php echo Helpers::get($item, 'button.title') ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>