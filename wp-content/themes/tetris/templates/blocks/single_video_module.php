<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="single-video <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="single-video__bg"></div>
    <div class="max-width-min container-fluid-min">
        <div class="single-video__wrapper">
            <figure class="cu_video__wrap single-video__wrapper__link">
                <video src="<?php echo Helpers::get($subFields, 'content.video') ?>" playsinline loop muted class="cu_video"></video>
                <div class="cu_video__control__play single-video__wrapper__play" type="button" data-state="play">
                    <svg xmlns="http://www.w3.org/2000/svg" width="45" height="52" viewBox="0 0 45 52" fill="none">
                        <path d="M45 26L-2.447e-06 51.9808L-1.75686e-07 0.0192356L45 26Z" fill="white"/>
                    </svg>
                </div>
                <div class="cu_video__control__progress__wrap">
                    <progress class="cu_video__control__progress" value="0" min="0">
                    </progress>
                    <span class="cu_video__control__progress_bar"></span>
                </div>
                <div class="cu_video__control__fullscreen" type="button" data-state="go-fullscreen">
                    <img src="<?php echo get_template_directory_uri() . '/dest/images/fullscreen.png' ?>">
                </div>
            </figure>

        </div>
    </div>
</section>
