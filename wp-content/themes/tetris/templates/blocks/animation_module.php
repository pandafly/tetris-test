<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="animation <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="animation__wrapper desktop">
            <div class="animation__bg"></div>
            <div class="animation__labels">
                <?php if (Helpers::get($subFields, 'content.repeater')): ?>
                    <div class="animation__labels__inner">
                        <?php foreach (Helpers::get($subFields, 'content.repeater') as $key => $item): ?>
                            <span data-key="<?php echo $key ?>">
                                <?php echo $item['label'] ?? '' ?>
                            </span>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div data-file="<?php echo Helpers::get($subFields, 'content.json_file') ?>" class="animation__wrapper__lottie"></div>
            <div class="animation__wrapper__content animate-loop">
                <?php if (Helpers::get($subFields, 'content.repeater')): ?>
                    <div class="animate-loop__inner">
                        <?php foreach (Helpers::get($subFields, 'content.repeater') as $item): ?>
                            <div class="animate-loop__inner__wrapper">
                                <div class="animate-loop__inner__single">
                                    <?php echo $item['text'] ?? '' ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="animation__transparent"></div>
        </div>
        <div class="animation__mobile">
            <?php if (Helpers::get($subFields, 'content.repeater')): ?>
                <div class="animation__mobile__loop">
                    <?php foreach (Helpers::get($subFields, 'content.repeater') as $item): ?>
                        <div class="animation__mobile__loop__single">
                            <?php echo $item['text'] ?? '' ?>
                            <?php echo wp_get_attachment_image($item['mobile_image'], 'full') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>