<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$getQuotes = Helpers::get($subFields, 'content.items');
?>

<section class="quotes <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="quotes__wrapper">
            <div class="quotes__wrapper__head">
                <div class="quotes__wrapper__head__text">
                    <span><?php echo Helpers::get($subFields, 'content.label') ?></span>
                    <h2><?php echo Helpers::get($subFields, 'content.title') ?></h2>
                </div>
                <div class="quotes__wrapper__head__nav">
                    <?php if (count($getQuotes) > 1): ?>
                        <div class="swiper_nav">
                            <button class="quotes__prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M22 13.5C22.8284 13.5 23.5 12.8284 23.5 12C23.5 11.1716 22.8284 10.5 22 10.5V13.5ZM0.939341 10.9393C0.353554 11.5251 0.353554 12.4749 0.939341 13.0607L10.4853 22.6066C11.0711 23.1924 12.0208 23.1924 12.6066 22.6066C13.1924 22.0208 13.1924 21.0711 12.6066 20.4853L4.12132 12L12.6066 3.51472C13.1924 2.92893 13.1924 1.97919 12.6066 1.3934C12.0208 0.807611 11.0711 0.807611 10.4853 1.3934L0.939341 10.9393ZM22 10.5L2 10.5V13.5L22 13.5V10.5Z"
                                          fill="#1A1A1A"/>
                                </svg>
                            </button>
                            <button class="quotes__next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M2 10.5C1.17157 10.5 0.5 11.1716 0.5 12C0.5 12.8284 1.17157 13.5 2 13.5L2 10.5ZM23.0607 13.0607C23.6464 12.4749 23.6464 11.5251 23.0607 10.9393L13.5147 1.3934C12.9289 0.807612 11.9792 0.807612 11.3934 1.3934C10.8076 1.97919 10.8076 2.92893 11.3934 3.51472L19.8787 12L11.3934 20.4853C10.8076 21.0711 10.8076 22.0208 11.3934 22.6066C11.9792 23.1924 12.9289 23.1924 13.5147 22.6066L23.0607 13.0607ZM2 13.5L22 13.5L22 10.5L2 10.5L2 13.5Z"
                                          fill="#1A1A1A"/>
                                </svg>
                            </button>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="quotes__wrapper__body">
                <?php if ($getQuotes): ?>
                    <div class="quotes__wrapper__body quotes__loop slider">
                        <div class="swiper-wrapper">
                            <?php foreach ($getQuotes as $item):
                                $fields = get_fields($item); ?>
                                <div class="swiper-slide">
                                    <div class="quotes__loop__single">
                                        <div class="quotes__loop__single__inner">
                                            <div class="quotes__loop__single__inner__image">
                                                <?php echo get_the_post_thumbnail($item, 'full') ?>
                                            </div>
                                            <div class="quotes__loop__descr">
                                                <div class="quotes__loop__descr__head">
                                                    <h3><?php echo $fields['quote']['description']['title'] ?? '' ?></h3>
                                                    <span><?php echo $fields['quote']['description']['label'] ?? '' ?></span>
                                                </div>
                                                <div class="quotes__loop__descr__content">
                                                    <?php echo $fields['quote']['description']['description'] ?? '' ?>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="quotes__loop__single__title"><?php echo get_the_title($item) ?></h4>
                                        <?php if (Helpers::get($fields, 'quote.link.title')): ?>
                                            <a class="quotes__loop__single__link"
                                               target="<?php echo Helpers::get($fields, 'quote.link.target') ?: '_self'; ?>"
                                               href="<?php echo Helpers::get($fields, 'quote.link.url') ?>">
                                                <?php echo Helpers::get($fields, 'quote.link.title') ?>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="16" viewBox="0 0 26 16" fill="none">
                                                    <path d="M1 7C0.447715 7 0 7.44772 0 8C0 8.55228 0.447715 9 1 9V7ZM25.7071 8.70711C26.0976 8.31658 26.0976 7.68342 25.7071 7.29289L19.3431 0.928932C18.9526 0.538408 18.3195 0.538408 17.9289 0.928932C17.5384 1.31946 17.5384 1.95262 17.9289 2.34315L23.5858 8L17.9289 13.6569C17.5384 14.0474 17.5384 14.6805 17.9289 15.0711C18.3195 15.4616 18.9526 15.4616 19.3431 15.0711L25.7071 8.70711ZM1 9H25V7H1V9Z"
                                                          fill="#1A1A1A"/>
                                                </svg>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="quotes__wrapper__body quotes__loop grid">
                        <?php foreach ($getQuotes as $item):
                            $fields = get_fields($item); ?>
                            <div class="quotes__loop__single">
                                <div class="quotes__loop__single__inner">
                                    <div class="quotes__loop__single__inner__image">
                                        <?php echo get_the_post_thumbnail($item, 'full') ?>
                                    </div>
                                    <div class="quotes__loop__descr">
                                        <div class="quotes__loop__descr__head">
                                            <h3><?php echo $fields['quote']['description']['title'] ?? '' ?></h3>
                                            <span><?php echo $fields['quote']['description']['label'] ?? '' ?></span>
                                        </div>
                                        <div class="quotes__loop__descr__content">
                                            <?php echo $fields['quote']['description']['description'] ?? '' ?>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="quotes__loop__single__title"><?php echo get_the_title($item) ?></h4>
                                <?php if (Helpers::get($fields, 'quote.link.title')): ?>
                                    <a class="quotes__loop__single__link"
                                       target="<?php echo Helpers::get($fields, 'quote.link.target') ?: '_self'; ?>"
                                       href="<?php echo Helpers::get($fields, 'quote.link.url') ?>">
                                        <?php echo Helpers::get($fields, 'quote.link.title') ?>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="16" viewBox="0 0 26 16" fill="none">
                                            <path d="M1 7C0.447715 7 0 7.44772 0 8C0 8.55228 0.447715 9 1 9V7ZM25.7071 8.70711C26.0976 8.31658 26.0976 7.68342 25.7071 7.29289L19.3431 0.928932C18.9526 0.538408 18.3195 0.538408 17.9289 0.928932C17.5384 1.31946 17.5384 1.95262 17.9289 2.34315L23.5858 8L17.9289 13.6569C17.5384 14.0474 17.5384 14.6805 17.9289 15.0711C18.3195 15.4616 18.9526 15.4616 19.3431 15.0711L25.7071 8.70711ZM1 9H25V7H1V9Z"
                                                  fill="#1A1A1A"/>
                                        </svg>
                                    </a>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>