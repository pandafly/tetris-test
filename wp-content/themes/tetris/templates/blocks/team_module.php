<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$teammates = get_posts(['post_type' => 'team', 'post_status' => 'publish', 'numberposts' => -1]);
?>

<section class="team <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="team__wrapper">
            <h2><?php echo Helpers::get($subFields, 'content.title') ?></h2>
            <div class="team__wrapper__loop">
                <?php if ($teammates): ?>
                    <?php foreach ($teammates as $item):
                        $fields = get_fields($item->ID); ?>
                        <div class="team__wrapper__loop__single">
                            <div class="team__wrapper__loop__single__img">
                                <?php echo get_the_post_thumbnail($item->ID, 'full') ?>
                            </div>
                            <div>
                                <span><?php echo Helpers::get($fields, 'team.label') ?></span>
                                <h4><?php echo get_the_title($item->ID) ?></h4>
                                <a href="tel:<?php echo Helpers::get($fields, 'team.tel') ?>">
                                    <?php echo Helpers::get($fields, 'team.tel') ?>
                                </a>
                                <a href="mailto:<?php echo Helpers::get($fields, 'team.email') ?>">
                                    <?php echo Helpers::get($fields, 'team.email') ?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
