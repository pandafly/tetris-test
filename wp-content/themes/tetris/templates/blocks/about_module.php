<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="about <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="about__wrapper">
            <div class="about__wrapper__inner">
                <h2><?php echo Helpers::get($subFields, 'content.title') ?></h2>
                <div class="about__wrapper__inner__descr">
                    <?php echo Helpers::get($subFields, 'content.description') ?>
                    <a class="changable"
                       href="<?php echo Helpers::get($subFields, 'content.link.url') ?>"
                       target="<?php echo Helpers::get($subFields, 'content.link.target') ?: '_self' ?>">
                        <?php echo Helpers::get($subFields, 'content.link.title'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
