<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
?>

<section class="full-vertical" <?php echo $bgAdd; ?>>
    <div class="full-vertical__wrapper">
        <div class="full-vertical__wrapper__label">
            <span>
                <?php echo Helpers::get($subFields, 'content.label') ?>
            </span>
        </div>
        <div class="full-vertical__contents">
            <?php if (Helpers::get($subFields, 'content.repeater')): ?>
                <?php foreach (Helpers::get($subFields, 'content.repeater') as $item):
                    $changeColors = CustomFunctions::changeColor(Helpers::get($item, 'colors'));
                    $colors = CustomFunctions::colorsControl(Helpers::get($item, 'colors')); ?>
                    <div class="full-vertical__contents__single <?php echo $changeColors ?>" <?php echo $colors ?>>
                        <div class="full-vertical__contents__single__inner">
                            <div class="full-vertical__contents__single__text"><?php echo $item['content'] ?></div>
                            <?php if (Helpers::get($item, 'link_enable')): ?>
                                <a class="changable"
                                   href="<?php echo Helpers::get($item, 'link.url') ?>"
                                   target="<?php echo Helpers::get($item, 'link.target') ?: '_self' ?>">
                                    <?php echo Helpers::get($item, 'link.title'); ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="full-vertical__images">
            <div class="full-vertical__images__inner">
                <?php foreach (Helpers::get($subFields, 'content.repeater') as $item): ?>
                    <section class="full-vertical__images__single">
                        <?php echo wp_get_attachment_image($item['image'], 'full') ?>
                    </section>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="full-vertical__mobile">
        <?php if (Helpers::get($subFields, 'content.repeater')): ?>
            <?php foreach (Helpers::get($subFields, 'content.repeater') as $item):
                $changeColors = CustomFunctions::changeColor(Helpers::get($item, 'colors'));
                $colors = CustomFunctions::colorsControl(Helpers::get($item, 'colors')); ?>
                <div class="full-vertical__mobile__single <?php echo $changeColors ?>" <?php echo $colors ?>>
                    <?php echo wp_get_attachment_image($item['image'], 'full') ?>
                    <div class="full-vertical__mobile__single__text">
                        <?php echo $item['content'] ?>
                        <?php if (Helpers::get($item, 'link_enable')): ?>
                            <a class="changable"
                               href="<?php echo Helpers::get($item, 'link.url') ?>"
                               target="<?php echo Helpers::get($item, 'link.target') ?: '_self' ?>">
                                <?php echo Helpers::get($item, 'link.title'); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>