<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="values <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="values__wrapper">
            <div class="values__wrapper__label">
                <span><?php echo Helpers::get($subFields, 'content.label'); ?></span>
            </div>
            <div class="values__wrapper__loop">
                <?php if (Helpers::get($subFields, 'content.items')): ?>
                    <?php foreach (Helpers::get($subFields, 'content.items') as $item): ?>
                        <div class="values__wrapper__loop__single">
                            <h2><?php echo $item['title'] ?></h2>
                            <div class="values__wrapper__loop__single__descr">
                                <?php echo $item['value'] ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
