<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$reverseBlock = $subFields['content']['reverse'] ?? '';
?>

<section class="image-text <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="image-text__wrapper <?php echo $reverseBlock ? 'reverse' : ''; ?>">
            <div class="image-text__wrapper__content">
                <span class="image-text__wrapper__content__label">
                    <?php echo Helpers::get($subFields, 'content.label'); ?>
                </span>
                <div class="image-text__wrapper__content__inner">
                    <div class="image-text__wrapper__content__descr">
                        <?php echo Helpers::get($subFields, 'content.description'); ?>
                    </div>
                    <?php if (Helpers::get($subFields, 'content.button.enable')): ?>
                        <a class="changable" target="<?php echo Helpers::get($subFields, 'content.button.link.target') ?: '_self' ?>"
                           href="<?php echo Helpers::get($subFields, 'content.button.link.url') ?>">
                            <?php echo Helpers::get($subFields, 'content.button.link.title'); ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="image-text__wrapper__image">
                <?php echo wp_get_attachment_image(Helpers::get($subFields, 'content.image'), 'full') ?>
            </div>
        </div>
    </div>
</section>
