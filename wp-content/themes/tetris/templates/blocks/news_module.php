<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$getAllNews = get_posts(['post_type' => 'news', 'post_status' => 'publish', 'numberposts' => -1]);
$getNews = get_posts(['post_type' => 'news', 'post_status' => 'publish', 'numberposts' => Helpers::get($subFields, 'content.posts_count')]);
$readMore = Helpers::get($subFields, 'content.post_read_more');
$mobSlider = !Helpers::get($subFields, 'content.load_more') ? 'slider' : '';
$miniHeader = Helpers::get($subFields, 'content.smaller_title') ? 'mini' : '';
?>

<section class="news-posts <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min <?php echo $mobSlider; ?>">
        <div class="news-posts__wrapper <?php echo $mobSlider; ?>">
            <div class="news-posts__wrapper__head <?php echo $mobSlider ?>">
                <h3><?php echo Helpers::get($subFields, 'content.title'); ?></h3>
                <?php if (count($getNews) > 1 && $mobSlider) : ?>
                    <div class="swiper_nav">
                        <button class="news-slider__prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="23" viewBox="0 0 24 23" fill="none">
                                <path d="M2 10.0811C1.17157 10.0811 0.5 10.7526 0.5 11.5811C0.5 12.4095 1.17157 13.0811 2 13.0811L2 10.0811ZM23.0607 12.6417C23.6464 12.0559 23.6464 11.1062 23.0607 10.5204L13.5147 0.974454C12.9289 0.388667 11.9792 0.388667 11.3934 0.974454C10.8076 1.56024 10.8076 2.50999 11.3934 3.09577L19.8787 11.5811L11.3934 20.0663C10.8076 20.6521 10.8076 21.6019 11.3934 22.1877C11.9792 22.7734 12.9289 22.7734 13.5147 22.1877L23.0607 12.6417ZM2 13.0811L22 13.0811L22 10.0811L2 10.0811L2 13.0811Z"
                                      fill="#1A1A1A"/>
                            </svg>
                        </button>
                        <button class="news-slider__next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="23" viewBox="0 0 24 23" fill="none">
                                <path d="M2 10.0811C1.17157 10.0811 0.5 10.7526 0.5 11.5811C0.5 12.4095 1.17157 13.0811 2 13.0811L2 10.0811ZM23.0607 12.6417C23.6464 12.0559 23.6464 11.1062 23.0607 10.5204L13.5147 0.974454C12.9289 0.388667 11.9792 0.388667 11.3934 0.974454C10.8076 1.56024 10.8076 2.50999 11.3934 3.09577L19.8787 11.5811L11.3934 20.0663C10.8076 20.6521 10.8076 21.6019 11.3934 22.1877C11.9792 22.7734 12.9289 22.7734 13.5147 22.1877L23.0607 12.6417ZM2 13.0811L22 13.0811L22 10.0811L2 10.0811L2 13.0811Z"
                                      fill="#1A1A1A"/>
                            </svg>
                        </button>
                    </div>
                <?php endif; ?>
            </div>
            <div class="news-posts__wrapper__content">
                <?php if ($getNews): ?>
                    <div class="news-posts__wrapper__content__loop <?php echo $mobSlider; ?>">
                        <?php foreach ($getNews as $item): ?>
                            <?php get_template_part('templates/parts/news', 'single', ['id' => $item->ID, 'more' => $readMore]) ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="news-posts__wrapper__content__slider <?php echo $mobSlider; ?>">
                        <div class="news-slider__wrapper news_slider">
                            <div class="swiper-wrapper">
                                <?php foreach ($getNews as $item): ?>
                                    <div class="swiper-slide">
                                        <a href="<?php echo get_the_permalink($item->ID) ?>" class="news-slider__wrapper__single">
                                            <?php echo get_the_post_thumbnail($item->ID, 'full') ?>
                                            <span><?php echo get_the_title($item->ID) ?></span>
                                            <p><?php echo get_the_excerpt($item->ID) ?></p>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <?php if (Helpers::get($subFields, 'content.load_more') && (count($getAllNews) > Helpers::get($subFields, 'content.posts_count'))): ?>
                        <div class="news-posts__wrapper__content__loadmore">
                            <a class="changable"
                               data-page="0"
                               data-more="<?php echo $readMore; ?>"
                               data-count="<?php echo Helpers::get($subFields, 'content.posts_count'); ?>">
                                <?php echo Helpers::get($subFields, 'content.load_more_text'); ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M13.5 2C13.5 1.17157 12.8284 0.5 12 0.5C11.1716 0.5 10.5 1.17157 10.5 2L13.5 2ZM10.9393 23.0607C11.5251 23.6464 12.4749 23.6464 13.0607 23.0607L22.6066 13.5147C23.1924 12.9289 23.1924 11.9792 22.6066 11.3934C22.0208 10.8076 21.0711 10.8076 20.4853 11.3934L12 19.8787L3.51472 11.3934C2.92893 10.8076 1.97918 10.8076 1.3934 11.3934C0.807611 11.9792 0.807611 12.9289 1.3934 13.5147L10.9393 23.0607ZM10.5 2L10.5 22L13.5 22L13.5 2L10.5 2Z"
                                          fill="#EA5600"/>
                                </svg>
                            </a>
                            <?php get_template_part('templates/parts/mini', 'loader') ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Helpers::get($subFields, 'content.link')): ?>
                        <div class="news-posts__wrapper__content__link">
                            <a
                                    class="changable"
                                    href="<?php echo Helpers::get($subFields, 'content.link_value.url') ?>"
                                    target="<?php echo Helpers::get($subFields, 'content.link_value.target') ?: '_self' ?>">
                                <?php echo Helpers::get($subFields, 'content.link_value.title'); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
