<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="text-reveal <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="text-reveal__wrapper">
            <div class="text-reveal__wrapper__text">
                <?php echo preg_replace('([ÆØÅæøåA,a-zA-Z.,!?0-9]+(?![^<]*>))', '<span class="reveal_span">$0</span>', Helpers::get($subFields, 'content.top_text')) ?>
                <div class="text-reveal__wrapper__text__inner">
                    <?php echo wp_get_attachment_image(Helpers::get($subFields, 'content.image'), 'full') ?>
                    <div class="text-reveal__wrapper__text__inner__right">
                        <?php echo preg_replace('([ÆØÅæøåA,a-zA-Z.,!?0-9]+(?![^<]*>))', '<span class="reveal_span">$0</span>', Helpers::get($subFields, 'content.right_text')) ?>
                    </div>
                </div>
                <!--                --><?php /*echo preg_replace("/([^\\s>])(?!(?:[^<>]*)?>)/u", "<span class='reveal_span'>$1</span>", Helpers::get($subFields, 'content.text')); */ ?>
            </div>
        </div>
    </div>
</section>
