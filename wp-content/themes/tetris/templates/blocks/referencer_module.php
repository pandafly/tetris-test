<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
$references = get_posts(['post_type' => 'referencer', 'numberposts' => -1, 'post_status' => 'publish']);
?>

<section class="referencer <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="referencer__wrapper">
            <div class="referencer__wrapper__head">
                <span><?php echo Helpers::get($subFields, 'content.label'); ?></span>
                <h2><?php echo Helpers::get($subFields, 'content.title'); ?></h2>
            </div>
            <?php if ($references): ?>
                <div class="referencer__wrapper__loop">
                    <?php foreach ($references as $item):
                        $fields = get_fields($item->ID) ?>
                        <div class="referencer__wrapper__loop__row referencer-item">
                            <div class="referencer-item__inner">
                                <div class="referencer-item__image">
                                    <?php echo get_the_post_thumbnail($item->ID, 'full') ?>
                                </div>
                                <div class="referencer-item__content">
                                    <div class="referencer-item__content__inner">
                                        <div class="referencer-item__content__head">
                                            <h3><?php echo get_the_title($item->ID); ?></h3>
                                            <h3><?php echo $fields['referencer']['city'] ?? ''; ?></h3>
                                        </div>
                                        <div class="referencer-item__content__body">
                                            <div class="referencer-item__content__body__descr">
                                                <?php echo $fields['referencer']['description'] ?? ''; ?>
                                            </div>
                                            <div class="referencer-item__table">
                                                <?php if ($fields['referencer']['table']): ?>
                                                    <?php foreach ($fields['referencer']['table'] as $row): ?>
                                                        <div class="referencer-item__table__row">
                                                            <span><?php echo $row['label'] ?? '' ?></span>
                                                            <span><?php echo $row['value'] ?? '' ?></span>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                <div class=""></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo $fields['referencer']['link'] ?? '' ?>">
                                <?php echo get_the_title($item->ID); ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                                    <path d="M0.868295 14.8114C0.282508 15.3972 0.282508 16.3469 0.868295 16.9327C1.45408 17.5185 2.40383 17.5185 2.98962 16.9327L0.868295 14.8114ZM17.5711 1.72994C17.5711 0.901508 16.8995 0.229935 16.0711 0.229936L2.57109 0.229935C1.74266 0.229935 1.07109 0.901507 1.07109 1.72993C1.07109 2.55836 1.74266 3.22994 2.57109 3.22994H14.5711V15.2299C14.5711 16.0584 15.2427 16.7299 16.0711 16.7299C16.8995 16.7299 17.5711 16.0584 17.5711 15.2299L17.5711 1.72994ZM2.98962 16.9327L17.1318 2.7906L15.0104 0.669275L0.868295 14.8114L2.98962 16.9327Z"
                                          fill="#1A1A1A"/>
                                </svg>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
