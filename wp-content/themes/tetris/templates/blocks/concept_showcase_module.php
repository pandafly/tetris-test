<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="concept <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="concept__wrapper">
        <div class="max-width-full container-fluid-min">
            <div class="concept__wrapper__head">
                <span><?php echo Helpers::get($subFields, 'content.label') ?></span>
                <h2><?php echo Helpers::get($subFields, 'content.title') ?></h2>
            </div>
            <?php if ($subFields['content']['tabs_show']): ?>
                <div class="concept__wrapper__tabs desktop">
                    <?php get_template_part('templates/parts/concept', 'tabs', Helpers::get($subFields, 'content.tabs')) ?>
                </div>
                <div class="concept__wrapper__tabs mobile">
                    <?php get_template_part('templates/parts/concept', 'tabsmob', Helpers::get($subFields, 'content.tabs')) ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($subFields['content']['slider_show']): ?>
            <div class="concept__wrapper__slider">
                <?php get_template_part('templates/parts/concept', 'slider', Helpers::get($subFields, 'content.slider.images')) ?>
            </div>
        <?php endif; ?>
    </div>
</section>
