<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

global $wp;
$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="single-text <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-min container-fluid-min">
        <div class="single-text__wrapper">
            <?php echo Helpers::get($subFields, 'content.text'); ?>
            <?php if (Helpers::get($subFields, 'content.social_links_enable')): ?>
            <?php endif; ?>
        </div>
    </div>
</section>
