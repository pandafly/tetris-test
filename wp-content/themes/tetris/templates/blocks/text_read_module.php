<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="text-read <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid">
        <div class="text-read__wrapper">
            <div class="text-read__wrapper__content">
                <?php if ($subFields['content']['rows']): ?>
                    <?php foreach ($subFields['content']['rows'] as $key => $item):
                        $dir = $key % 2 ? 'right' : ''; ?>
                        <p class="text-read__wrapper__content__row <?php echo $dir; ?>"><?php echo $item['text']; ?></p>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
