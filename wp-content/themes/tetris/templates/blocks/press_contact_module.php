<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$acfFieldKeys = [
    'content',
    'colors',
    'settings',
];
$subFields = CustomFunctions::getSubFields($acfFieldKeys);
$bgAdd = CustomFunctions::styleControl($subFields['settings']);
$changeColors = CustomFunctions::changeColor($subFields['colors']);
?>

<section class="press-contact <?php echo $changeColors ?>" <?php echo $bgAdd; ?>>
    <div class="max-width-full container-fluid-min">
        <div class="press-contact__wrapper">
            <div class="press-contact__wrapper__info">
                <h2><?php echo Helpers::get($subFields, 'content.left.title') ?></h2>
                <div class="press-contact__wrapper__info__descr">
                    <?php echo Helpers::get($subFields, 'content.left.description') ?>
                </div>
            </div>
            <div class="press-contact__wrapper__contacts">
                <?php echo wp_get_attachment_image(Helpers::get($subFields, 'content.right.image'), 'full') ?>
                <div class="press-contact__wrapper__contacts__descr press-contact__group">
                    <span><?php echo Helpers::get($subFields, 'content.right.address.label') ?></span>
                    <h4><?php echo Helpers::get($subFields, 'content.right.address.title') ?></h4>
                    <div class="press-contact__group__descr">
                        <?php echo Helpers::get($subFields, 'content.right.address.description') ?>
                    </div>
                    <div class="press-contact__group__contact-us">
                        <span><?php echo Helpers::get($subFields, 'content.right.contacts.label') ?></span>
                        <?php if (Helpers::get($subFields, 'content.right.contacts.repeater')): ?>
                            <?php foreach (Helpers::get($subFields, 'content.right.contacts.repeater') as $item):
                                if ($item['type'] == 1):?>
                                    <a class="press-contact__group__contact-us__mail" href="mailto:<?php echo Helpers::get($item, 'email') ?>">
                                        <?php echo Helpers::get($item, 'email') ?>
                                    </a>
                                <?php else: ?>
                                    <a href="tel:<?php echo Helpers::get($item, 'tel') ?>">
                                        <?php echo Helpers::get($item, 'tel') ?>
                                    </a>
                                <?php endif;
                            endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="press-contact__downloads">
            <?php if (Helpers::get($subFields, 'content.downloads')): ?>
                <div class="press-contact__downloads__wrapper">
                    <div class="press-contact__downloads__mobile">
                        <?php echo wp_get_attachment_image(Helpers::get($subFields, 'content.mobile.icon', 'full')) ?>
                        <div class="press-contact__downloads__mobile__descr">
                            <?php echo Helpers::get($subFields, 'content.mobile.description') ?>
                        </div>
                    </div>
                    <?php foreach (Helpers::get($subFields, 'content.downloads') as $item): ?>
                        <div class="press-contact__downloads__wrapper__col">
                            <h3><?php echo $item['title'] ?? '' ?></h3>
                            <?php if ($item['row']): ?>
                                <?php foreach ($item['row'] as $row): ?>
                                    <div class="press-contact__downloads__wrapper__row">
                                        <?php echo wp_get_attachment_image(Helpers::get($row, 'icon'), 'full') ?>
                                        <span><?php echo Helpers::get($row, 'label') ?></span>
                                        <?php if (Helpers::get($row, 'type') == 1): ?>
                                            <a download href="<?php echo Helpers::get($row, 'file') ?>">
                                                <?php echo Helpers::get($row, 'text') ?>
                                            </a>
                                        <?php else: ?>
                                            <a href="<?php echo Helpers::get($row, 'link.url') ?>"
                                               target="<?php echo Helpers::get($row, 'link.target') ?: '_self' ?>">
                                                <?php echo Helpers::get($row, 'link.title'); ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
