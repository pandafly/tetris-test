<?php
if (isset($args['id'])):?>
    <a href="<?php echo get_the_permalink($args['id']) ?>" class="news-posts__wrapper__content__single desktop">
        <?php echo get_the_post_thumbnail($args['id'], 'full') ?>
        <div class="news-posts__wrapper__text">
            <span><?php echo get_the_title($args['id']) ?></span>
            <p><?php echo get_the_excerpt($args['id']) ?></p>
        </div>
    </a>
    <div class="news-posts__wrapper__content__single mobile">
        <?php echo get_the_post_thumbnail($args['id'], 'full') ?>
        <div class="news-posts__wrapper__text">
            <div>
                <span><?php echo get_the_title($args['id']) ?></span>
                <p><?php echo get_the_excerpt($args['id']) ?></p>
            </div>
            <?php if ($args['more']): ?>
                <a class="changable" href="<?php echo get_the_permalink($args['id']) ?>">
                    <?php echo $args['more'] ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
