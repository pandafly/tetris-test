<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

if (isset($args)): ?>
    <div class="concept__wrapper__tabs__head">
        <div class="concept__wrapper__tab__wrap">
            <a class="concept__wrapper__tab tab_1">
                <?php echo $args['tab_1']['title'] ?? '' ?>
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="24" viewBox="0 0 23 24" fill="none">
                    <path d="M13.168 2C13.168 1.17157 12.4964 0.5 11.668 0.5C10.8395 0.5 10.168 1.17157 10.168 2L13.168 2ZM10.6073 23.0607C11.1931 23.6464 12.1428 23.6464 12.7286 23.0607L22.2746 13.5147C22.8604 12.9289 22.8604 11.9792 22.2746 11.3934C21.6888 10.8076 20.739 10.8076 20.1532 11.3934L11.668 19.8787L3.18269 11.3934C2.5969 10.8076 1.64715 10.8076 1.06137 11.3934C0.47558 11.9792 0.47558 12.9289 1.06137 13.5147L10.6073 23.0607ZM10.168 2L10.168 22L13.168 22L13.168 2L10.168 2Z"
                          fill="#1A1A1A"/>
                </svg>
            </a>
            <div class="concept__wrapper__hidden">
                <?php echo Helpers::get($args, 'tab_1.description') ?>
            </div>
        </div>
        <div class="concept__wrapper__tab__wrap">
            <a class="concept__wrapper__tab tab_2">
                <?php echo $args['tab_2']['title'] ?? '' ?>
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="24" viewBox="0 0 23 24" fill="none">
                    <path d="M13.168 2C13.168 1.17157 12.4964 0.5 11.668 0.5C10.8395 0.5 10.168 1.17157 10.168 2L13.168 2ZM10.6073 23.0607C11.1931 23.6464 12.1428 23.6464 12.7286 23.0607L22.2746 13.5147C22.8604 12.9289 22.8604 11.9792 22.2746 11.3934C21.6888 10.8076 20.739 10.8076 20.1532 11.3934L11.668 19.8787L3.18269 11.3934C2.5969 10.8076 1.64715 10.8076 1.06137 11.3934C0.47558 11.9792 0.47558 12.9289 1.06137 13.5147L10.6073 23.0607ZM10.168 2L10.168 22L13.168 22L13.168 2L10.168 2Z"
                          fill="#1A1A1A"/>
                </svg>
            </a>
            <div class="concept__wrapper__hidden">
                <?php if (Helpers::get($args, 'tab_2.items')): ?>
                    <?php foreach (Helpers::get($args, 'tab_2.items') as $item): ?>
                        <div class="concept__wrapper__tabs__facts__single">
                            <h5><?php echo $item['title'] ?? '' ?></h5>
                            <p><?php echo $item['value'] ?? '' ?></p>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <a class="concept__wrapper__tab tab_3"
           target="<?php echo Helpers::get($args, 'tab_3.link.target') ?: '_self'; ?>"
           href="<?php echo Helpers::get($args, 'tab_3.link.url') ?>">
            <?php echo Helpers::get($args, 'tab_3.link.title') ?>
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                <path d="M0.869027 15.0106C0.283241 15.5964 0.283241 16.5462 0.869027 17.1319C1.45481 17.7177 2.40456 17.7177 2.99035 17.1319L0.869027 15.0106ZM17.5718 1.92915C17.5718 1.10073 16.9002 0.429154 16.0718 0.429154L2.57182 0.429153C1.7434 0.429153 1.07182 1.10073 1.07182 1.92915C1.07182 2.75758 1.7434 3.42915 2.57182 3.42915H14.5718V15.4292C14.5718 16.2576 15.2434 16.9292 16.0718 16.9292C16.9003 16.9292 17.5718 16.2576 17.5718 15.4292L17.5718 1.92915ZM2.99035 17.1319L17.1325 2.98981L15.0112 0.868494L0.869027 15.0106L2.99035 17.1319Z"
                      fill="#1A1A1A"/>
            </svg>
        </a>
    </div>
<?php endif; ?>