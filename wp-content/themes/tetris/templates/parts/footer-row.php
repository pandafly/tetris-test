<?php if (isset($args)): ?>
    <div class="site-footer__wrapper__col__row <?php echo $args['type'] == 3 ? 'social' : '' ?>">
        <h3><?php echo $args['title'] ?? '' ?></h3>
        <?php if ($args['type'] == 0): ?>
            <div class="site-footer__row__descr"><?php echo $args['description'] ?></div>
        <?php endif; ?>
        <?php if ($args['type'] == 1): ?>
            <div class="site-footer__row__links">
                <?php if ($args['links']): ?>
                    <?php foreach ($args['links'] as $link): ?>
                        <a target="<?php echo $link['link']['target'] ?: '_self' ?>"
                           href="<?php echo $link['link']['url'] ?? '' ?>"><?php echo $link['link']['title'] ?? '' ?></a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if ($args['type'] == 2): ?>
            <?php if ($args['contacts']): ?>
                <?php foreach ($args['contacts'] as $contact):
                    if ($contact['type'] == 0):?>
                        <a class="site-footer__row__contact"
                           href="tel:<?php echo $contact['contact'] ?>"><?php echo $contact['contact'] ?></a>
                    <?php else: ?>
                        <a class="site-footer__row__contact"
                           href="mailto:<?php echo $contact['contact'] ?>"><?php echo $contact['contact'] ?></a>
                    <?php endif;
                endforeach; ?>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($args['type'] == 3): ?>
            <div class="site-footer__row__social">
                <?php if ($args['socials']): ?>
                    <?php foreach ($args['socials'] as $item): ?>
                        <a target="_blank" href="<?php echo $item['link'] ?>" class="site-footer__row__social__link">
                            <?php if ($item['icon']): ?>
                                <?php echo file_get_contents($item['icon']) ?>
                            <?php endif; ?>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>