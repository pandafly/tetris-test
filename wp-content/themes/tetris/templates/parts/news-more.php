<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$fields = get_fields('options');
$getNews = get_posts(['post_type' => 'news', 'post_status' => 'publish', 'exclude' => [get_the_id()], 'numberposts' => Helpers::get($fields, 'single_news.more_news.count')]);
$readMore = Helpers::get($fields, 'single_news.more_news.read_more');
$changeColors = CustomFunctions::changeColor(Helpers::get($fields, 'single_news.more_news.colors'));
$colors = CustomFunctions::colorsControl(Helpers::get($fields, 'single_news.more_news.colors'));
?>
<section class="news-posts single-post <?php echo $changeColors ?>" <?php echo $colors; ?>>
    <div class="max-width-full container-fluid-min slider">
        <div class="news-posts__wrapper slider">
            <div class="news-posts__wrapper__head slider">
                <h3><?php echo Helpers::get($fields, 'single_news.more_news.title'); ?></h3>
                <?php if (count($getNews) > 1) : ?>
                    <div class="swiper_nav">
                        <button class="news-slider__prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="23" viewBox="0 0 24 23" fill="none">
                                <path d="M2 10.0811C1.17157 10.0811 0.5 10.7526 0.5 11.5811C0.5 12.4095 1.17157 13.0811 2 13.0811L2 10.0811ZM23.0607 12.6417C23.6464 12.0559 23.6464 11.1062 23.0607 10.5204L13.5147 0.974454C12.9289 0.388667 11.9792 0.388667 11.3934 0.974454C10.8076 1.56024 10.8076 2.50999 11.3934 3.09577L19.8787 11.5811L11.3934 20.0663C10.8076 20.6521 10.8076 21.6019 11.3934 22.1877C11.9792 22.7734 12.9289 22.7734 13.5147 22.1877L23.0607 12.6417ZM2 13.0811L22 13.0811L22 10.0811L2 10.0811L2 13.0811Z"
                                      fill="#1A1A1A"/>
                            </svg>
                        </button>
                        <button class="news-slider__next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="23" viewBox="0 0 24 23" fill="none">
                                <path d="M2 10.0811C1.17157 10.0811 0.5 10.7526 0.5 11.5811C0.5 12.4095 1.17157 13.0811 2 13.0811L2 10.0811ZM23.0607 12.6417C23.6464 12.0559 23.6464 11.1062 23.0607 10.5204L13.5147 0.974454C12.9289 0.388667 11.9792 0.388667 11.3934 0.974454C10.8076 1.56024 10.8076 2.50999 11.3934 3.09577L19.8787 11.5811L11.3934 20.0663C10.8076 20.6521 10.8076 21.6019 11.3934 22.1877C11.9792 22.7734 12.9289 22.7734 13.5147 22.1877L23.0607 12.6417ZM2 13.0811L22 13.0811L22 10.0811L2 10.0811L2 13.0811Z"
                                      fill="#1A1A1A"/>
                            </svg>
                        </button>
                    </div>
                <?php endif; ?>
            </div>
            <div class="news-posts__wrapper__content">
                <?php if ($getNews): ?>
                    <div class="news-posts__wrapper__content__loop slider">
                        <?php foreach ($getNews as $item): ?>
                            <?php get_template_part('templates/parts/news', 'single', ['id' => $item->ID, 'more' => $readMore]) ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="news-posts__wrapper__content__slider slider">
                        <div class="news-slider__wrapper news_slider">
                            <div class="swiper-wrapper">
                                <?php foreach ($getNews as $item): ?>
                                    <div class="swiper-slide">
                                        <a href="<?php echo get_the_permalink($item->ID) ?>" class="news-slider__wrapper__single">
                                            <?php echo get_the_post_thumbnail($item->ID, 'full') ?>
                                            <span><?php echo get_the_title($item->ID) ?></span>
                                            <p><?php echo get_the_excerpt($item->ID) ?></p>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="news-posts__wrapper__content__link">
                        <a
                                class="changable"
                                href="<?php echo Helpers::get($fields, 'single_news.more_news.link.url') ?>"
                                target="<?php echo Helpers::get($fields, 'single_news.more_news.link.target') ?: '_self' ?>">
                            <?php echo Helpers::get($fields, 'single_news.more_news.link.title'); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>