<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$fields = get_fields('options');
?>
<section class="single-text__social__wrap">
    <div class="container-fluid max-width-news">
        <div class="single-text__wrapper__social">
            <h4><?php echo Helpers::get($fields, 'single_news.social.title') ?></h4>
            <?php if (Helpers::get($fields, 'single_news.social.icons')): ?>
                <div class="single-text__wrapper__social__loop">
                    <?php foreach (Helpers::get($fields, 'single_news.social.icons') as $item): ?>
                        <a target="_blank"
                           href="<?php echo $item['link'] ?>"
                           data-link="<?php echo home_url($wp->request); ?>"
                           class="single-text__wrapper__social__icon <?php echo $item['copy_link'] ? 'clipboard_link' : '' ?>">
                            <?php if ($item['icon']): ?>
                                <?php echo wp_get_attachment_image($item['icon'], 'full') ?>
                            <?php endif; ?>
                            <?php if ($item['copy_link']): ?>
                                <span class="text_copied">
                            <?php echo !empty($item['popup_text']) && $item['popup_text'] ? $item['popup_text'] : 'Link copied'; ?>
                        </span>
                            <?php endif; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>