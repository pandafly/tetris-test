var $ = jQuery;
$(document).ready(function () {

    var swiper = new Swiper(".news_slider", {
        slidesPerView: 'auto',
        loop         : false,
        autoHeight   : true,
        spaceBetween : 24,
        navigation   : {
            nextEl: '.news-slider__next',
            prevEl: '.news-slider__prev',
        },
        breakpoints  : {
            300: {
                slidesPerView: 1,
            },
            768: {
                slidesPerView: 'auto',
            },
        }
    });
    var swiper = new Swiper(".quotes__loop", {
        slidesPerView: 'auto',
        loop         : false,
        autoHeight   : true,
        spaceBetween : 27,
        navigation   : {
            nextEl: '.quotes__next',
            prevEl: '.quotes__prev',
        },
    });

    var dataYears = document.querySelectorAll('.timeline-slider');
    if (dataYears.length) {
        dataYears.forEach((elem, key) => {
            var yearsStr = elem.dataset.years ?? '';
            var yearsArr = yearsStr.split(',');
            var swiper = new Swiper(elem, {
                slidesPerView: 'auto',
                loop         : false,
                autoHeight   : false,
                speed        : 700,
                spaceBetween : 80,
                navigation   : {
                    nextEl: '.timeline__next',
                    prevEl: '.timeline__prev',
                },
                pagination   : {
                    el          : ".timeline__pagina",
                    clickable   : true,
                    renderBullet: function (index, className) {
                        return '<div class="' + className + ' timeline__pagina__item"><div class="timeline__pagina__line"></div><div class="timeline__pagina__wrapper"><span class="timeline__pagina__year">' + yearsArr[index] + '</span><span class="timeline__pagina__bullet"></span></div></div>';
                    },
                },
                on           : {
                    slideChange: function () {
                        var paginaWidth = 0;
                        var paginasWrapper = elem.querySelector('.timeline__pagina');
                        var paginas = elem.querySelectorAll('.timeline__pagina .timeline__pagina__item');
                        if (paginas.length) {
                            paginas.forEach(e => {
                                paginaWidth += e.offsetWidth;
                            })
                        }
                        var oneChangeWidth = -1 * ((paginaWidth - paginasWrapper.offsetWidth) / (paginas.length - 1));
                        console.log((oneChangeWidth * swiper.realIndex), oneChangeWidth, swiper.realIndex)

                        paginasWrapper.style.transform = 'translateX(' + (oneChangeWidth * swiper.realIndex) + 'px)';
                        elem.classList.add('slide_' + swiper.realIndex)
                        elem.classList.remove('slide_' + this.previousIndex)
                    },
                },
                breakpoints  : {
                    300 : {
                        spaceBetween: 16,
                    },
                    768 : {
                        spaceBetween: 40,
                    },
                    1024: {
                        spaceBetween: 80,
                    },
                }
            });
        });
    }

    var swiper = new Swiper(".concept-slider", {
        spaceBetween  : -150,
        slidesPerView : 'auto',
        speed         : 500,
        loop          : true,
        centeredSlides: true,
        navigation    : {
            nextEl: '.concept-slider__next',
            prevEl: '.concept-slider__prev',
        },
        pagination    : {
            el            : ".concept-slider__pagination",
            dynamicBullets: true,
            clickable     : true
        },
        breakpoints   : {
            300 : {
                spaceBetween: -30,
            },
            769 : {
                spaceBetween: -50,
            },
            1200: {
                spaceBetween: -50,
            },
            1400: {
                spaceBetween: -90,
            },
            1700: {
                spaceBetween: -150,
            },
        }
    });
    var getActiveHead = document.querySelector('.head__wrapper__sub-pages.mobile .head__wrapper__sub-pages__single.active')
    var swiperHead = new Swiper(".head_slider", {
        spaceBetween : 16,
        slidesPerView: 'auto',
        speed        : 500,
    });
    if (getActiveHead) {
        swiperHead.slideTo(getActiveHead.dataset.key);
    }
});
