document.addEventListener('DOMContentLoaded', function () {
    var $ = jQuery;

    /// Header link hover ///
    (() => {

        var link = document.querySelector('.site-header__wrapper__link__inner .visible_link');
        if (!link) {
            return;
        }
        link.addEventListener('mouseover', (event) => {
            setTimeout(function () {
                link.querySelector('svg').style.opacity = 1;
            }, 200)
        });
        link.addEventListener('mouseout', (event) => {
            link.querySelector('svg').style.opacity = 0;
        });
    })();

    /// Smooth scroll ///
    (() => {

        var vSlider = document.querySelector('section.v-slider'),
            elems = document.querySelectorAll('.v-slider__content__list .v-slider__content__list__single'),
            textSection = document.querySelectorAll('.text-reveal .text-reveal__wrapper__text');

        var height = 0;
        var offsetHeightReveal = [];
        if (vSlider) {
            elems.forEach(e => {
                height += e.offsetHeight;
            })
            vSlider.style.height = height + 500 + 'px';
        }

        if (textSection.length) {
            textSection.forEach((e, key) => {
                offsetHeightReveal[key] = e.offsetTop;
            })
        }

        // const scroller = new LocomotiveScroll({
        //     el          : document.querySelector('.scroller'),
        //     smooth      : true,
        //     multiplier  : .7,
        //     repeat      : true,
        //     getDirection: true,
        //     lerp        : .1
        // });
        // document.querySelector('.scroller').style.height = '100%';

        var header = document.querySelector('.site-header__fixed'),
            sections = document.querySelectorAll('main section'),
            imagesWrapper = document.querySelector('.v-slider__wrapper__images'),
            images = document.querySelectorAll('.v-slider__images__list .v-slider__images__list__single'),
            heroTxtAbove = document.querySelector('.hero-module__layer-above__inner'),
            heroTxtUnder = document.querySelector('.hero-module__layer-under__content'),
            heroSection = document.querySelector('section#hero');

        if (vSlider) {
            var offsetHeight = 0;
            if (sections) {
                for (var i = 0; i < sections.length; i++) {
                    var el = sections[i];
                    if (el.classList.contains('v-slider')) {
                        break;
                    }
                    offsetHeight += el.offsetHeight;
                }
            }

            var startTxt = 0;
            var startImg = images.length - 1;
            scroller.on('scroll', (srollEvent) => {
                var pos = srollEvent.scroll.y;

                /// Reveal text ///
                if (textSection.length) {
                    textSection.forEach((elem, key) => {
                        var revealSpans = elem.querySelectorAll('.reveal_span')
                        var moduleHeight = elem.offsetHeight;
                        if (pos + window.innerHeight > offsetHeightReveal[key]) {
                            var percent = ((pos + window.innerHeight - offsetHeightReveal[key]) / (moduleHeight + window.innerHeight / 2)) * 100;
                            revealSpans.forEach((span, num) => {
                                var numPercent = (num / revealSpans.length) * 100;
                                if (numPercent < percent) {
                                    if (percent - numPercent < 1.5) {
                                        span.style.opacity = .4;
                                    } else if (percent - numPercent < 2.5) {
                                        span.style.opacity = .6;
                                    } else if (percent - numPercent < 3.5) {
                                        span.style.opacity = .8;
                                    } else {
                                        span.style.opacity = 1;
                                    }

                                } else {
                                    span.style.opacity = .15;
                                }
                            })
                        }
                    });
                }

                /// Fixed header ///

                if (header) {
                    if (pos > 100) {
                        if (srollEvent.direction == 'up') {
                            header.classList.add('colored')
                            header.classList.remove('hide')
                        } else {
                            header.classList.add('hide')
                            header.classList.remove('colored')
                        }
                    } else {
                        header.classList.remove('hide')
                        header.classList.remove('colored')
                    }
                }

                /// hero module ///

                if (heroSection.offsetHeight <= heroTxtUnder.offsetHeight + pos) {
                    heroTxtUnder.style.transform = 'translateY(' + Math.round(heroSection.offsetHeight - heroTxtUnder.offsetHeight) + 'px)';
                } else {
                    heroTxtAbove.style.transform = 'translateY(' + Math.round(pos) + 'px)';
                    heroTxtUnder.style.transform = 'translateY(' + Math.round(pos) + 'px)';
                }

                /// vertcical slider ///

                var elemsHeight = 0;
                for (var i = 0; i < elems.length; i++) {
                    var element = elems[i];
                    if (element.classList.contains('active')) {
                        break;
                    }
                    elemsHeight += element.offsetHeight;
                }
                var changedPos = pos - offsetHeight - elemsHeight;
                if (pos - offsetHeight > 0) {
                    imagesWrapper.style.transform = 'translateY(' + (pos - offsetHeight) + 'px)';
                }
                if (pos - offsetHeight + imagesWrapper.offsetHeight > height + 500) {
                    imagesWrapper.style.transform = 'translateY(' + (height + 500 - imagesWrapper.offsetHeight) + 'px)';
                }
                var activeImage = document.querySelector('.v-slider__images__list__single.active');
                var activeEl = document.querySelector('.v-slider__content__list__single.active');
                var activeElementHeight = activeEl.offsetHeight;
                var percent = changedPos / activeElementHeight * 100;
                var percentInPx = (activeImage.offsetHeight - (startTxt + 1) * 50) * (percent / 100);
                if (percentInPx > activeImage.offsetHeight - (startTxt + 1) * 50) {
                    if (images[startImg - 1]) {
                        startTxt++;
                        startImg--;
                        activeImage.classList.add('scrolled');
                        activeEl.classList.remove('active');
                        activeImage.classList.remove('active');
                        elems[startTxt].classList.add('active');
                        images[startImg].classList.add('active');
                    }
                    percentInPx = activeImage.offsetHeight - startTxt * 50;
                }
                if (srollEvent.direction == 'up') {
                    if (percent < 0) {
                        if (images[startImg + 1]) {
                            startTxt--;
                            startImg++;
                            activeEl.classList.remove('active');
                            activeImage.classList.remove('active');
                            activeImage.classList.remove('scrolled');
                            elems[startTxt].classList.add('active');
                            images[startImg].classList.add('active');
                            images[startImg].classList.remove('scrolled');
                        }
                        percentInPx = 0;

                    }
                }
                activeImage.style.transform = 'translateY(-' + percentInPx + 'px)';
            });


        }
    })();
})