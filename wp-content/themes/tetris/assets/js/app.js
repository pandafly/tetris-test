document.addEventListener('DOMContentLoaded', function () {
    var $ = jQuery;

    var footer = document.querySelector('footer.site-footer');
    var scrollWrapper = document.querySelector('.scroll-content');
    var scrollerWrapper = document.querySelector('.scroller');

    function changeHeightAfterDOMInsertElems() {
        scrollWrapper.style.height = scrollerWrapper.offsetHeight + footer.offsetHeight + 'px';
    }

    /// Slider tabs module ///

    (() => {
        var tabsHead = document.querySelectorAll('.concept__wrapper__tabs.desktop');
        if (!tabsHead.length) {
            return;
        }
        if (window.innerWidth > 768) {
            tabsHead.forEach(e => {
                var tab1 = e.querySelector('.tab_1');
                var tabContent1 = e.querySelector('.tab_body_1');
                var tab2 = e.querySelector('.tab_2');
                var tabContent2 = e.querySelector('.tab_body_2');
                var bodyWrapper = e.querySelector('.concept__wrapper__tabs__body__inner');
                bodyWrapper.style.height = tabContent1.offsetHeight + 'px';
                changeHeightAfterDOMInsertElems()
                if (tab1) {
                    tab1.addEventListener('click', () => {
                        tab1.classList.add('active');
                        tab2.classList.remove('active');
                        bodyWrapper.classList.remove('move');
                        tabContent2.classList.add('hidden');
                        tabContent1.classList.remove('hidden');
                        bodyWrapper.style.height = tabContent1.offsetHeight + 'px';
                        setTimeout(function () {
                            changeHeightAfterDOMInsertElems()
                        }, 300)
                    })
                }
                if (tab2) {
                    tab2.addEventListener('click', () => {
                        tab2.classList.add('active');
                        tab1.classList.remove('active');
                        bodyWrapper.classList.add('move');
                        tabContent1.classList.add('hidden');
                        tabContent2.classList.remove('hidden');
                        bodyWrapper.style.height = tabContent2.offsetHeight + 'px';
                        setTimeout(function () {
                            changeHeightAfterDOMInsertElems()
                        }, 300)
                    })
                }
            })
        }
    })();

    /// Mobile Accordion///
    (() => {
        $('.concept__wrapper__tabs__head .concept__wrapper__tab__wrap a.concept__wrapper__tab').on('click', function () {
            if ($(this).hasClass('open')) {
                $(this).siblings('.concept__wrapper__hidden').slideUp();
                $(this).removeClass('open');
                $(this).closest('.concept__wrapper__tab__wrap').toggleClass('open');
                setTimeout(function () {
                    changeHeightAfterDOMInsertElems()
                }, 300)
            } else {
                $(this).closest('.concept__wrapper__tabs__head').find('.concept__wrapper__hidden').slideUp();
                $('.concept__wrapper__tabs__head .concept__wrapper__tab__wrap a.concept__wrapper__tab').removeClass('open');
                $(this).siblings('.concept__wrapper__hidden').slideToggle();
                $(this).toggleClass('open');
                $(this).closest('.concept__wrapper__tab__wrap').toggleClass('open');
                setTimeout(function () {
                    changeHeightAfterDOMInsertElems()
                }, 300)
            }
        });
    })();


    /// Video module ///
    // (() => {
    //     var playButton = document.querySelectorAll('.single-video__wrapper__play');
    //     if (!playButton.length) {
    //         return;
    //     }
    //
    //     playPauseAfterLoad(playButton);
    //     playButton.forEach(e => {
    //         var video = e.nextElementSibling;
    //         video.addEventListener('click', function (event) {
    //             if (event.target != e) {
    //                 video.pause();
    //                 e.classList.remove("hidden");
    //             }
    //         })
    //         e.addEventListener('click', function () {
    //             handlePlayButton(video, e)
    //         })
    //     })
    // })();
    //
    // function playPauseAfterLoad(playButton)
    // {
    //     if (!playButton) {
    //         return;
    //     }
    //
    //     setTimeout(() => {
    //         playButton.forEach(e => {
    //             var video = e.parentNode.querySelector('video')
    //             console.log(video)
    //             if (video) {
    //                 var playPromise = video.play();
    //                 if (playPromise !== undefined) {
    //                     playPromise.then(_ => {
    //                         setTimeout(() => {
    //                             video.pause();
    //                         }, 1)
    //                     })
    //                         .catch(error => {
    //                             console.log(error + 'play video error')
    //                         });
    //                 }
    //             }
    //         })
    //     }, 100)
    // }
    //
    // async function playVideo(video)
    // {
    //     try {
    //         await video.play();
    //     } catch (err) {
    //         console.log(err)
    //     }
    // }
    //
    // function handlePlayButton(video, playBtn)
    // {
    //     if (video.paused) {
    //         playBtn.classList.add("hidden");
    //         playVideo(video);
    //     } else {
    //         video.pause();
    //         playBtn.classList.remove("hidden");
    //     }
    // }

    /// Copy to clipboard ///

    (() => {
        var linkCopy = document.querySelector('.clipboard_link');
        if (!linkCopy) {
            return;
        }
        linkCopy.addEventListener('click', e => {
            e.preventDefault();
            var popup = linkCopy.querySelector('.text_copied');
            if (!popup.classList.contains('copied')) {
                popup.classList.add('copied')
            }
            setTimeout(function () {
                if (popup.classList.contains('copied')) {
                    popup.classList.remove('copied')
                }
            }, 1250)
            var aa = appCopyToClipBoard(linkCopy.dataset.link);
        })
    })();

    function appCopyToClipBoard(sText) {
        var oText = false, bResult = false;
        try {
            oText = document.createElement("textarea");
            $(oText).addClass('clipboardCopier').val(sText).insertAfter('body');
            oText.select();
            document.execCommand("Copy");
            bResult = true;
        } catch (e) {
        }

        $(oText).remove();
        return bResult;
    }

    /// Loadmore news ///
    (() => {
        var btns = document.querySelectorAll('.news-posts__wrapper__content__loadmore a');
        if (!btns.length) {
            return;
        }
        btns.forEach(btn => {
            btn.addEventListener('click', () => {

                var loader = btn.nextElementSibling;
                btn.dataset.page = +btn.dataset.page + 1;
                loader.classList.add('active')
                var data = new FormData();
                data.append('action', "newsLoadmore");
                data.append('page', btn.dataset.page);
                data.append('count', btn.dataset.count);
                data.append('more', btn.dataset.more);
                (async () => {
                    var response = await fetch(filters_ajax.url, {
                        method: 'POST', body: data
                    });
                    var result = await response.json();
                    var parser = new DOMParser();
                    if (result !== null) {
                        var doc = parser.parseFromString(result.res, 'text/html');
                        var posts = doc.querySelectorAll('.news-posts__wrapper__content__single');
                        var wrapper = btn.closest('.news-posts__wrapper__content').querySelector('.news-posts__wrapper__content__loop');
                        setTimeout(function () {
                            if (posts.length) {
                                posts.forEach(elem => {
                                    wrapper.append(elem)
                                })
                            }
                            var imgs = document.querySelectorAll('.news-posts__wrapper__content__single > img')
                            imgs.forEach(img => {
                                const srcset = img.getAttribute('srcset');
                                img.setAttribute('srcset', '');
                                img.setAttribute('srcset', srcset);
                            });
                            loader.classList.remove('active')
                            if (result.btn == 'false') {
                                btn.closest('.news-posts__wrapper__content__loadmore').style.display = 'none';
                            }
                            changeHeightAfterDOMInsertElems()
                        }, 500)
                    } else {
                        console.log('error');
                    }
                })();
            })
        })
    })();

    (() => {
        var supportsVideo = !!document.createElement("video").canPlayType
        if (supportsVideo) {
            var videoContainer = document.querySelectorAll(".cu_video__wrap")
            videoContainer.forEach(function (el) {
                var video = el.querySelector(".cu_video"),
                    videoControls = el.querySelector(".cu_video__control")

                video.controls = false;

                var play = el.querySelector('.cu_video__control__play'),
                    progress = el.querySelector('.cu_video__control__progress'),
                    progressBar = el.querySelector('.cu_video__control__progress_bar'),
                    progressWrap = el.querySelector('.cu_video__control__progress__wrap'),
                    fullscreen = el.querySelector('.cu_video__control__fullscreen')

                playPauseAfterLoad(play)
                var fullScreenEnabled = !!(document.fullscreenEnabled
                    || document.mozFullScreenEnabled || document.msFullscreenEnabled
                    || document.webkitSupportsFullscreen || document.webkitFullscreenEnabled
                    || document.createElement('video').webkitRequestFullScreen);
                if (!fullScreenEnabled) {
                    // fullscreen.style.display = 'none';
                    // progressWrap.classList.add('removed')
                }

                video.addEventListener('play', function () {
                    play.classList.add('active')
                })
                video.addEventListener('pause', function () {
                    play.classList.remove('active')
                })
                video.addEventListener('ended', function () {
                    play.classList.remove('active')
                })
                video.addEventListener("click", function () {
                    video.pause();
                });
                play.addEventListener("click", function () {
                    if (video.paused || video.ended) {
                        video.play();
                    } else {
                        video.pause();
                    }
                });

                /*mute.addEventListener("click", () => {
                    video.muted = !video.muted;
                });*/

                video.addEventListener("loadedmetadata", () => {
                    progress.setAttribute("max", video.duration);
                });

                video.addEventListener("timeupdate", () => {
                    if (!progress.getAttribute("max"))
                        progress.setAttribute("max", video.duration);
                    progress.value = video.currentTime;
                    progressBar.style.width = `${Math.floor(
                        (video.currentTime * 100) / video.duration
                    )}%`;
                });

                progress.addEventListener("click", (e) => {
                    const rect = progress.getBoundingClientRect();
                    const pos = (e.pageX - rect.left) / progress.offsetWidth;
                    video.currentTime = pos * video.duration;
                });

                fullscreen.addEventListener('click', function (e) {
                    handleFullscreen();
                });

                var setFullscreenData = function (state) {
                    el.setAttribute('data-fullscreen', !!state);
                    if (el.dataset.fullscreen === 'true') {
                        fullscreen.classList.add('active')
                    } else {
                        fullscreen.classList.remove('active')
                    }
                }

                var isFullScreen = function () {
                    return !!(document.fullScreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
                }
                var fullVideoWrapper = document.querySelector('.video_full');
                var elWrapper = el.closest('.single-video__wrapper');
                var handleFullscreen = function () {
                    if (isFullScreen() || elWrapper.classList.contains('full')) {
                        if (window.innerWidth < 768 && fullScreenEnabled) {
                            if (document.exitFullscreen) document.exitFullscreen();
                            else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
                            else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
                            else if (document.msExitFullscreen) document.msExitFullscreen();
                            setFullscreenData(false);
                        } else {
                            elWrapper.append(el)
                            fullVideoWrapper.style.zIndex = -1;
                            elWrapper.classList.remove('full')
                        }
                    } else {
                        if (window.innerWidth < 768 && fullScreenEnabled) {
                            if (el.requestFullscreen) el.requestFullscreen();
                            else if (el.mozRequestFullScreen) el.mozRequestFullScreen();
                            else if (el.webkitRequestFullScreen) {
                                video.webkitRequestFullScreen();
                            } else if (el.msRequestFullscreen) el.msRequestFullscreen();
                            setFullscreenData(true);
                        } else {
                            if (fullVideoWrapper) {
                                fullVideoWrapper.style.zIndex = 121;
                                elWrapper.classList.add('full')
                                fullVideoWrapper.append(el)
                            }
                        }
                    }
                }

                document.addEventListener('fullscreenchange', function (e) {
                    setFullscreenData(!!(document.fullScreen || document.fullscreenElement));
                });
                document.addEventListener('webkitfullscreenchange', function (event) {
                    setFullscreenData(!!document.webkitIsFullScreen);
                });
                document.addEventListener('mozfullscreenchange', function () {
                    setFullscreenData(!!document.mozFullScreen);
                });
                document.addEventListener('msfullscreenchange', function () {
                    setFullscreenData(!!document.msFullscreenElement);
                });
            })
        }
    })();


})

function playPauseAfterLoad(playButton) {
    if (!playButton) {
        return;
    }

    setTimeout(() => {
        var video = playButton.parentNode.querySelector('video')
        if (video) {
            var playPromise = video.play();
            if (playPromise !== undefined) {
                playPromise.then(_ => {
                    setTimeout(() => {
                        video.pause();
                    }, 1)
                })
                    .catch(error => {
                        console.log(error + 'play video error')
                    });
            }
        }
    }, 100)
}


//Vertical slider 100vh using GSAP ScrollTrigger with changing bg color

document.addEventListener("DOMContentLoaded", () => {
    gsap.registerPlugin(ScrollTrigger);

    const slides = document.querySelectorAll('.v-slider2__slide');

    initVerticalSlider2();

    window.addEventListener('resize', function (event) {
        initVerticalSlider2();
    });

    function initVerticalSlider2() {
        if (window.innerWidth > 1024) {
            slides.forEach((el, i) => {
                const imgWrapper = el.querySelector('.v-slider2__image__wrapper');

                if (i < slides.length - 1 && i > 0) {
                    ScrollTrigger.create({
                        trigger: imgWrapper,
                        pin: true,
                        start: "top top",
                        end: "bottom top",
                        onUpdate: (self) => {
                            changeBackgroundColor(i, self.progress);
                        }
                    });
                } else if (i !== 0) {
                    ScrollTrigger.create({
                        trigger: imgWrapper,
                        pin: true,
                        start: "top top",
                        end: "bottom top",
                        onEnter: () => {
                            slides[slides.length - 2].querySelector('.v-slider2__image__wrapper').parentNode.classList.add('v-slider2__image__wrapper-top');
                            imgWrapper.parentNode.classList.add('v-slider2__image__wrapper-bottom');
                        },
                        onEnterBack: () => {
                            slides[slides.length - 2].querySelector('.v-slider2__image__wrapper').parentNode.classList.add('v-slider2__image__wrapper-top');
                            imgWrapper.parentNode.classList.add('v-slider2__image__wrapper-bottom');
                        },
                        onLeave: () => {
                            slides[slides.length - 2].querySelector('.v-slider2__image__wrapper').parentNode.classList.remove('v-slider2__image__wrapper-top');
                        },
                        onLeaveBack: () => {
                            slides[slides.length - 2].querySelector('.v-slider2__image__wrapper').parentNode.classList.remove('v-slider2__image__wrapper-top');
                            imgWrapper.parentNode.classList.remove('v-slider2__image__wrapper-bottom');
                        },
                        onUpdate: (self) => {
                            changeBackgroundColor(i, self.progress);
                        }
                    });
                }
            });
        }
    }

    function changeBackgroundColor(i, progress) {
        const wrapper = document.querySelector('.v-slider2__wrapper');

        if (progress > 0.5) {
            wrapper.style.backgroundColor = `rgb(${slides[i].dataset.bgcolor})`;
        }
    }
});


//Sticky label with JS (position:sticky doesn't work because of Smooth Scroll)

document.addEventListener("DOMContentLoaded", () => {
    const outerWrapper = document.querySelector('.v-slider2__outer-wrapper');
    const label = document.querySelector('.v-slider2__label');

    ScrollTrigger.create({
        trigger: label,
        pin: outerWrapper,
        start: "top 24px",
        end: `+=${outerWrapper.offsetHeight}px`
    });
});