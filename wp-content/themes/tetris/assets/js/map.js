function initMap()
{
    var maps = document.getElementById('map');
    if (maps) {
        var lat = +maps.dataset.lat;
        var lng = +maps.dataset.lng;
        var myLatLng = {lat, lng};
        var theme = filters_ajax.theme_uri;
        console.log(maps.dataset.iconMob)
        var markers = {
            icon: window.innerWidth > 768 ? maps.dataset.icon : maps.dataset.iconMob,
        };
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat, lng},
            zoom  : 16,
            styles: [
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers"    : [
                        {
                            "color": "#dbdbdb"
                        }
                    ]
                },
                {
                    featureType: "poi.park",
                    elementType: "geometry",
                    stylers    : [{color: "#c6c6c6"}],
                },
                {elementType: "labels.text.fill", stylers: [{color: "#746855"}]},
                {
                    "elementType": "labels.icon",
                    "stylers"    : [
                        {
                            "color": "#7e7e7e",
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers"    : [
                        {
                            "color": "#7e7e7e",
                        }
                    ]
                },

                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers"    : [
                        {
                            "visibility": "on"
                        }
                    ]
                },

                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers"    : [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers"    : [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers"    : [
                        {
                            "color": "#b3b3b3"
                        },
                        {
                            "visibility": "off"
                        },
                        {
                            "saturation": "-1"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers"    : [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers"    : [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#dbdbdb"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers"    : [
                        {
                            "color": "#7e7e7e"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers"    : [
                        {
                            "color": "#e5e5e5"
                        },
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers"    : [
                        {
                            "color": "#b7b6b6"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers"    : [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers"    : [
                        {
                            "visibility": "off"
                        }
                    ]
                },
            ]
        });
        var popup = document.querySelector('.contact-map__popup');
        var infowindow = new google.maps.InfoWindow({
            content: popup,
        });
        var infoWindowContent = infowindow.content;
        var cityMarker = new google.maps.Marker({
            position: myLatLng,
            icon    : markers.icon,
            map,
            title   : "Map Marker",
        });

        infoWindowContent.querySelector('.contact-map__popup__inner__close').addEventListener('click', () => {
            closePopup(infoWindowContent, infowindow, popup);
        })

        cityMarker.addListener("click", () => {
            infowindow.open({
                anchor     : cityMarker,
                map,
                shouldFocus: false,
            });
            popup.classList.add('active')
        });
    }
}

function closePopup(infoWindowContent, infowindow, popup)
{
    popup.classList.add('closed');
    infoWindowContent.classList.add('closed');
    setTimeout(() => {
        infoWindowContent.classList.remove('opened', 'closed');
        infowindow.close();
        popup.classList.remove('closed');
    }, 800)
}
