class ModalPlugin extends Scrollbar.ScrollbarPlugin
{
    static pluginName = 'modal';

    static defaultOptions = {
        open: true,
    };

    transformDelta(delta)
    {
        return this.options.open ? {x: 0, y: 0} : delta;
    }
}

class MyPlugin extends Scrollbar.ScrollbarPlugin
{
    static pluginName = 'myPlugin';

    onUpdate()
    {
        console.log('scrollbar updated');

        Scrollbar.ScrollbarPlugin.update();
    }
}

class MobilePlugin extends Scrollbar.ScrollbarPlugin
{
    static pluginName = 'mobile';
    static defaultOptions = {
        speed: 0.5,
    };

    transformDelta(delta, fromEvent)
    {
        if (fromEvent.type !== 'touchend') {
            return delta;
        }

        return {
            x: delta.x * this.options.speed,
            y: delta.y * this.options.speed,
        };
    }
}

document.addEventListener('DOMContentLoaded', function () {
    gsap.registerPlugin(ScrollTrigger);
    Scrollbar.use(ModalPlugin);
    Scrollbar.use(MobilePlugin);
    // Scrollbar.use(MyPlugin);
    var header = document.querySelector('.site-header__fixed'),
        footer = document.querySelector('footer.site-footer'),
        pos = 0,
        direction,
        isVisible,
        sections = document.querySelectorAll('main section'),
        vSlider = document.querySelectorAll('section.v-slider.scroll.desktop'),
        textSection = document.querySelectorAll('.text-reveal .text-reveal__wrapper__text'),
        textSectionWrap = document.querySelectorAll('.text-reveal'),
        loader = document.querySelector('.loader'),
        mainContent = document.querySelector('main'),
        mobileOverlay = document.querySelector('.site-header__mobile__overlay'),
        heroWrapper = document.querySelector('section.hero-module'),
        scrollWrapper = document.querySelector('.scroll-content'),
        scrollerWrapper = document.querySelector('.scroller'),
        menuBtn = document.querySelector('.site-header__mobile'),
        headerMenu = document.querySelector('header.site-header'),
        errorPage = document.querySelector('main.error-page'),
        fullScreen = document.querySelectorAll('.cu_video__control__fullscreen'),
        menuOpen = false,
        fullVideo = false;

    function changeHeightAfterDOMInsertElems()
    {
        scrollWrapper.style.height = scrollerWrapper.offsetHeight + footer.offsetHeight + 'px';
    }


    var footerOffset = footer.offsetTop;
    var startTxt = 0;
    var vSliderTop = [];
    var offsetHeightReveal = [];
    var startImg = [];
    vSlider.forEach(e => {
        var images = e.querySelectorAll('.v-slider__images__list .v-slider__images__list__single');
        var style = e.currentStyle || window.getComputedStyle(e);
        vSliderTop.push(e.offsetTop + parseInt(style.paddingTop));
        startImg.push(images.length - 1);
    })
    if (textSectionWrap.length) {
        textSectionWrap.forEach((e, key) => {
            offsetHeightReveal[key] = e.offsetTop;
        })
    }


    /// scrollbar init ///
    var bodyScrollBar = Scrollbar.init(document.body, {
        damping: 0.08, delegateTo: document,
        plugins: {
            mobile: {
                speed: 0.5,
            },
        },
    });

    bodyScrollBar.updatePluginOptions('modal', {open: false});
    if (menuBtn) {
        menuBtn.addEventListener('click', () => {
            if (!menuOpen) {
                bodyScrollBar.updatePluginOptions('modal', {open: true});
                headerMenu.classList.add('open');
                menuOpen = true;
            } else {
                bodyScrollBar.updatePluginOptions('modal', {open: false});
                headerMenu.classList.remove('open');
                menuOpen = false;
            }
        })
    }

    if (fullScreen.length) {
        fullScreen.forEach(full => {
            full.addEventListener('click', () => {
                if (!fullVideo) {
                    bodyScrollBar.updatePluginOptions('modal', {open: true});
                    fullVideo = true;
                } else {
                    bodyScrollBar.updatePluginOptions('modal', {open: false});
                    fullVideo = false;
                }
            })
        })
    }
    ScrollTrigger.scrollerProxy(".scroller", {
        scrollTop(value)
        {
            if (arguments.length) {
                bodyScrollBar.scrollTop = value;
            }
            return bodyScrollBar.scrollTop;
        }
    });
    bodyScrollBar.addListener(ScrollTrigger.update);
    ScrollTrigger.defaults({scroller: '.scroller', start: "top " + (window.innerHeight / 2 + 400)});

    var scrollWrapper = document.querySelector('.scroll-content');
    var scrollerWrapper = document.querySelector('.scroller');
    if (window.innerWidth > 550 && !errorPage) {
        footer.style.height = footer.offsetHeight + 'px';
        scrollWrapper.style.height = scrollWrapper.offsetHeight + footer.offsetHeight + 'px';
    }
    imagesLoaded(mainContent, function () {
        setTimeout(function () {
            loader.classList.add('loaderAway');
        }, 1200);
    });
    vertSlider();
    animateLottie();
    window.addEventListener('resize', function (event) {
        changeHeightAfterDOMInsertElems();
        ScrollTrigger.refresh(true)
    }, true);

    // batch(".team__wrapper__loop__single", {
    //     start      : "top-=" + (window.innerHeight),
    //     end        : "bottom top",
    //     markers    : false,
    //     interval   : 0.1,
    //     batchMax   : 4,
    //     onEnter    : batch => gsap.to(batch, {autoAlpha: 1, stagger: 0.15, overwrite: true}),
    //     onLeave    : batch => gsap.set(batch, {autoAlpha: 0, overwrite: true}),
    //     onEnterBack: batch => gsap.to(batch, {autoAlpha: 1, stagger: 0.15, overwrite: true}),
    //     onLeaveBack: batch => gsap.set(batch, {autoAlpha: 0, overwrite: true})
    // });

    gsap.defaults({ease: "power3"});
    gsap.set(".team__wrapper__loop__single", {y: 100});

    ScrollTrigger.batch(".team__wrapper__loop__single", {
        //interval: 0.1, // time window (in seconds) for batching to occur.
        //batchMax: 3,   // maximum batch size (targets)
        onEnter    : batch => gsap.to(batch, {opacity: 1, y: 0, stagger: {each: 0.15, grid: [1, 3]}, overwrite: true}),
        onLeave    : batch => gsap.set(batch, {opacity: 0, y: -100, overwrite: true}),
        onEnterBack: batch => gsap.to(batch, {opacity: 1, y: 0, stagger: 0.15, overwrite: true}),
        onLeaveBack: batch => gsap.set(batch, {opacity: 0, y: 100, overwrite: true})
    });

    ScrollTrigger.addEventListener("refreshInit", () => gsap.set(".team__wrapper__loop__single", {y: 0}));

    /// Smooth scroll event ///

    bodyScrollBar.addListener(status => {
        loader.style.top = status.offset.y + 'px';
        if (document.querySelector('.video_full')) {
            document.querySelector('.video_full').style.top = status.offset.y + 'px';
        }
        changeHeightAfterDOMInsertElems();
        if (status.offset.y > pos) {
            direction = 'down';
        } else {
            direction = 'up';
        }
        pos = status.offset.y;

        /// Fixed header ///

        if (header) {
            header.style.top = status.offset.y + 'px';
            var mediaQuaries = window.innerWidth > 1024 ? 100 : 0;
            if (pos > mediaQuaries) {
                if (direction == 'up') {
                    header.classList.add('colored')
                    header.classList.remove('hide')
                } else {
                    header.classList.add('hide')
                    header.classList.remove('colored')
                }
            } else {
                header.classList.remove('hide')
                header.classList.remove('colored')
            }
        }

        /// Hero module ///

        if (heroWrapper) {
            var underContent = heroWrapper.querySelector('.hero-module__layer-under__content');
            var aboveContent = heroWrapper.querySelector('.hero-module__layer-above__inner');
            aboveContent.style.top = status.offset.y + 'px';
            if (status.offset.y < heroWrapper.offsetHeight - underContent.offsetHeight) {
                underContent.style.top = status.offset.y + 'px';
            } else {
                underContent.style.top = 'initial';
                underContent.style.bottom = 0;
            }
        }


        /// Fixed footer ///
        if (footer) {
            footer.style.top = (footerOffset + status.offset.y) + 'px';
        }

        if (mobileOverlay) {
            // mobileOverlay.style.top = status.offset.y + 'px';
        }

        /// Reveal text ///
        if (textSection.length) {
            textSection.forEach((elem, key) => {
                var revealSpans = elem.querySelectorAll('.reveal_span')
                var moduleHeight = elem.offsetHeight;
                if (pos + window.innerHeight > offsetHeightReveal[key]) {
                    var percent = ((pos + window.innerHeight - offsetHeightReveal[key]) / (moduleHeight + window.innerHeight / 2)) * 100;
                    revealSpans.forEach((span, num) => {
                        var numPercent = (num / revealSpans.length) * 100;
                        if (numPercent < percent) {
                            if (percent - numPercent < 1.5) {
                                span.style.opacity = .4;
                            } else if (percent - numPercent < 2.5) {
                                span.style.opacity = .6;
                            } else if (percent - numPercent < 3.5) {
                                span.style.opacity = .8;
                            } else {
                                span.style.opacity = 1;
                            }

                        } else {
                            span.style.opacity = .15;
                        }
                    })
                }
            });
        }

        /// vertcical slider ///
        vSlider.forEach((el, key) => {
            var images = el.querySelectorAll('.v-slider__images__list .v-slider__images__list__single');
            var imagesWrapper = el.querySelector('.v-slider__wrapper__images');
            var imagesWrapperInner = el.querySelector('.v-slider__images__wrap');
            var elems = el.querySelectorAll('.v-slider__content__list .v-slider__content__list__single');
            var height = 0;
            var offsetHeight = vSliderTop[key];
            var elemsHeight = 0;
            for (var i = 0; i < elems.length; i++) {
                var element = elems[i];
                if (element.classList.contains('active')) {
                    break;
                }
                elemsHeight += element.offsetHeight;
            }
            if (el) {
                elems.forEach(e => {
                    height += e.offsetHeight;
                })
                el.style.height = height + (window.innerHeight / 2) + 'px';
            }
            changeHeightAfterDOMInsertElems();
            var changedPos = pos - offsetHeight - elemsHeight;
            if (pos - offsetHeight > 0) {
                imagesWrapper.style.top = (pos - offsetHeight) + 'px';
            }
            if (pos - offsetHeight + imagesWrapper.offsetHeight > height + (window.innerHeight / 2)) {
                imagesWrapper.style.top = (height + (window.innerHeight / 2) - imagesWrapper.offsetHeight) + 'px';
            }
            var activeImage = document.querySelector('.v-slider__images__list__single.active');
            var activeEl = document.querySelector('.v-slider__content__list__single.active');
            var activeElementHeight = activeEl.offsetHeight;
            var percent = changedPos / activeElementHeight * 100;
            var percentInPx = (activeImage.offsetHeight - (startTxt + 1) * 50) * (percent / 100);
            if (percentInPx > activeImage.offsetHeight - (startTxt + 1) * 50) {
                if (images[startImg[key] - 1]) {
                    startTxt++;
                    startImg[key]--;
                    activeImage.classList.add('scrolled');
                    activeEl.classList.remove('active');
                    activeImage.classList.remove('active');
                    elems[startTxt].classList.add('active');
                    images[startImg[key]].classList.add('active');
                }
                percentInPx = activeImage.offsetHeight - startTxt * 50;
            }
            if (direction == 'up') {
                if (percent < 0) {
                    if (images[startImg[key] + 1]) {
                        startTxt--;
                        startImg[key]++;
                        activeEl.classList.remove('active');
                        activeImage.classList.remove('active');
                        activeImage.classList.remove('scrolled');
                        elems[startTxt].classList.add('active');
                        images[startImg[key]].classList.add('active');
                        images[startImg[key]].classList.remove('scrolled');
                    }
                    percentInPx = 0;

                }
            }
            activeImage.style.transform = 'translateY(-' + percentInPx + 'px)';
        })
    });

    /// Text read ///
    (() => {
        const backTextWrapper = document?.querySelectorAll("section.text-read");

        backTextWrapper.forEach((section) => {

            const textLines = section?.querySelectorAll(".text-read__wrapper__content__row");
            const textLineAnim = gsap.timeline({
                defaults     : {
                    duration: 2,
                    ease    : 'none'
                },
                scrollTrigger: {
                    trigger: section,
                    start  : "top-=" + window.innerHeight,
                    end    : "bottom top",
                    scrub  : 0.1,
                    // markers: true,
                }
            })

            textLineAnim
                .fromTo(textLines, {
                    x: (index, target) => index % 2 ? (window.innerWidth / 4) : (-window.innerWidth / 4)
                }, {
                    x: 0
                }, 0)
                .fromTo(textLines, {
                    x: 0
                }, {
                    x              : (index, target) => index % 2 ? (-window.innerWidth / 4) : (window.innerWidth / 4),
                    immediateRender: false
                }, 2)
        })
    })();

    (() => {

        gsap.utils.toArray(".text-slider__loop__row").forEach((line, i) => {
            const speed = 1;
            var direction = line.dataset.direction ? false : true;
            const links = line.querySelectorAll("h3"),
                tl = horizontalLoop(links, {speed: speed, reversed: direction, repeat: -1});
        });

        function horizontalLoop(items, config)
        {
            items = gsap.utils.toArray(items);
            config = config || {};
            let tl = gsap.timeline({
                    repeat           : config.repeat,
                    paused           : config.paused,
                    defaults         : {ease: "none"},
                    onReverseComplete: () => tl.totalTime(tl.rawTime() + tl.duration() * 100)
                }),
                length = items.length,
                startX = items[0].offsetLeft,
                times = [],
                widths = [],
                xPercents = [],
                curIndex = 0,
                pixelsPerSecond = (config.speed || 1) * 100,
                snap = config.snap === false ? (v) => v : gsap.utils.snap(config.snap || 1),
                totalWidth,
                curX,
                distanceToStart,
                distanceToLoop,
                item,
                i;
            gsap.set(items, {
                xPercent: (i, el) => {
                    let w = (widths[i] = parseFloat(gsap.getProperty(el, "width", "px")));
                    xPercents[i] = snap(
                        (parseFloat(gsap.getProperty(el, "x", "px")) / w) * 100 +
                        gsap.getProperty(el, "xPercent")
                    );
                    return xPercents[i];
                }
            });
            gsap.set(items, {x: 0});
            totalWidth =
                items[length - 1].offsetLeft +
                (xPercents[length - 1] / 100) * widths[length - 1] -
                startX +
                items[length - 1].offsetWidth *
                gsap.getProperty(items[length - 1], "scaleX") +
                (parseFloat(config.paddingRight) || 0);
            for (i = 0; i < length; i++) {
                item = items[i];
                curX = (xPercents[i] / 100) * widths[i];
                distanceToStart = item.offsetLeft + curX - startX;
                distanceToLoop =
                    distanceToStart + widths[i] * gsap.getProperty(item, "scaleX");
                tl.to(
                    item,
                    {
                        xPercent: snap(((curX - distanceToLoop) / widths[i]) * 100),
                        duration: distanceToLoop / pixelsPerSecond
                    },
                    0
                )
                    .fromTo(
                        item,
                        {
                            xPercent: snap(
                                ((curX - distanceToLoop + totalWidth) / widths[i]) * 100
                            )
                        },
                        {
                            xPercent       : xPercents[i],
                            duration       :
                                (curX - distanceToLoop + totalWidth - curX) / pixelsPerSecond,
                            immediateRender: false
                        },
                        distanceToLoop / pixelsPerSecond
                    )
                    .add("label" + i, distanceToStart / pixelsPerSecond);
                times[i] = distanceToStart / pixelsPerSecond;
            }

            function toIndex(index, vars)
            {
                vars = vars || {};
                Math.abs(index - curIndex) > length / 2 &&
                (index += index > curIndex ? -length : length); // always go in the shortest direction
                let newIndex = gsap.utils.wrap(0, length, index),
                    time = times[newIndex];
                if (time > tl.time() !== index > curIndex) {
                    vars.modifiers = {time: gsap.utils.wrap(0, tl.duration())};
                    time += tl.duration() * (index > curIndex ? 1 : -1);
                }
                curIndex = newIndex;
                vars.overwrite = true;
                return tl.tweenTo(time, vars);
            }

            tl.next = (vars) => toIndex(curIndex + 1, vars);
            tl.previous = (vars) => toIndex(curIndex - 1, vars);
            tl.current = () => curIndex;
            tl.toIndex = (index, vars) => toIndex(index, vars);
            tl.times = times;
            tl.progress(1, true).progress(0, true);
            if (config.reversed) {
                tl.vars.onReverseComplete();
                tl.reverse();
            }
            return tl;
        }
    })();
})

function vertSlider()
{
    gsap.to(".full-vertical__images__single", {
        yPercent     : -100,
        ease         : "none",
        stagger      : 0.5,
        scrollTrigger: {
            trigger: ".full-vertical__images__inner",
            start  : "top top",
            end    : "bottom",
            scrub  : true,
            pin    : true,
            markers: false,
        }
    });

    var getTextItemsWrapper = document.querySelector('.full-vertical__contents');
    if (!getTextItemsWrapper) {
        return;
    }
    var getTextItems = document.querySelectorAll('.full-vertical__contents .full-vertical__contents__single');
    var colorFirstItem = getCssInlineVariable(getTextItems[0]);
    getTextItemsWrapper.style.backgroundColor = colorFirstItem;
    gsap.utils.toArray(".full-vertical__contents__single").forEach((e, index) => {
        gsap.to(e, {
            scrollTrigger: {
                trigger    : e,
                start      : "top-=400 top",
                end        : "bottom-=400",
                markers    : false,
                scrub      : true,
                onEnter    : () => {
                    var prevItem = getTextItems[index];
                    if (prevItem) {
                        var color = getCssInlineVariable(prevItem);
                        getTextItemsWrapper.style.backgroundColor = color;
                    }
                },
                onEnterBack: () => {
                    var prevItem = getTextItems[index];
                    if (prevItem) {
                        var color = getCssInlineVariable(prevItem);
                        getTextItemsWrapper.style.backgroundColor = color;
                    }
                },
            },
        });
    })

    gsap.timeline({
        scrollTrigger: {
            trigger   : getTextItemsWrapper,
            pin       : '.full-vertical__wrapper__label',
            pinSpacing: false,
            start     : "top top",
            markers   : false,
            end       : "+=" + (getTextItemsWrapper.offsetHeight - window.innerHeight),
        },
    })


    gsap.set(".full-vertical__images__single", {zIndex: (i, target, targets) => targets.length - i});


}


function animateLottie()
{
    var animateWrapper = document.querySelector('.animation__wrapper.desktop');
    if (!animateWrapper || window.innerWidth < 1024) {
        return;
    }
    var animateWrapperInner = document.querySelector('.animate-loop__inner');
    var animationFile = document.querySelector('.animation__wrapper__lottie');
    var labels = document.querySelectorAll('.animation__labels__inner span');
    gsap.utils.toArray(".animate-loop__inner__single").forEach((e, index) => {
        gsap.to(e, {
            scrollTrigger: {
                trigger    : e,
                start      : "top-=500 top",
                end        : "bottom-=200",
                markers    : false,
                scrub      : true,
                onEnter    : () => {
                    e.classList.add('active')
                    labels.forEach(elem => {
                        elem.classList.remove('active')
                    })
                    labels[index].classList.add('active')
                },
                onLeave    : () => {
                    e.classList.remove('active')
                    labels[index].classList.remove('active')
                },
                onEnterBack: () => {
                    e.classList.add('active')
                    labels.forEach(elem => {
                        elem.classList.remove('active')
                    })
                    labels[index].classList.add('active')
                },
                onLeaveBack: () => {
                    e.classList.remove('active')
                    labels[index].classList.remove('active')
                }
            },
        });
    })

    gsap.timeline({
        scrollTrigger: {
            trigger   : '.animation__wrapper',
            pin       : '.animation__bg',
            pinSpacing: false,
            start     : "top top",
            markers   : false,
            end       : "+=" + (animateWrapperInner.offsetHeight - window.innerHeight),
        },
    })
    gsap.timeline({
        scrollTrigger: {
            trigger   : '.animation__wrapper',
            pin       : '.animation__labels',
            pinSpacing: true,
            start     : "top top",
            markers   : false,
            end       : "+=" + (animateWrapperInner.offsetHeight - window.innerHeight),
        },
    })
    gsap.timeline({
        scrollTrigger: {
            trigger   : '.animation__wrapper',
            pin       : '.animation__transparent',
            pinSpacing: true,
            start     : "top top",
            markers   : false,
            end       : "+=" + (animateWrapperInner.offsetHeight - window.innerHeight),
        },
    })
    if (animationFile) {
        ScrollTrigger.refresh(true)
        LottieScrollTrigger({
            target : animationFile,
            path   : animationFile.dataset.file,
            speed  : "slow",
            start  : "top top",
            end    : "+=" + (animateWrapperInner.offsetHeight - window.innerHeight),
            scrub  : true,
            markers: false
        });
    }
}

function LottieScrollTrigger(vars)
{
    let playhead = {frame: 0},
        target = gsap.utils.toArray(vars.target)[0],
        speeds = {slow: "+=2000", medium: "+=1000", fast: "+=500"},
        st = {
            trigger: target,
            pin    : true,
            start  : "top top",
            end    : speeds[vars.speed] || "+=1000",
            scrub  : 1
        },
        animation = lottie.loadAnimation({
            container: target,
            renderer : vars.renderer || "svg",
            loop     : false,
            autoplay : false,
            path     : vars.path
        });
    for (let p in vars) {
        st[p] = vars[p];
    }
    animation.addEventListener("DOMLoaded", function () {
        gsap.to(playhead, {
            frame        : animation.totalFrames - 1,
            ease         : "none",
            onUpdate     : () => animation.goToAndStop(playhead.frame, true),
            scrollTrigger: st
        });
        ScrollTrigger.sort();
        ScrollTrigger.refresh();
    });
    return animation;
}

function getCssInlineVariable(item)
{
    if (item) {
        const cssText = item.style.cssText;
        const rotationProp = cssText.split("; ").find(style => style.includes("bg-color-radio"));
        const rotationValue = rotationProp.slice(rotationProp.indexOf(":") + 1, rotationProp.indexOf(";"));
        return rotationValue;
    }
}

function batch(targets, vars)
{
    let varsCopy = {},
        interval = vars.interval || 0.1,
        proxyCallback = (type, callback) => {
            let batch = [],
                delay = gsap.delayedCall(interval, () => {
                    callback(batch);
                    batch.length = 0;
                }).pause();
            return self => {
                batch.length || delay.restart(true);
                batch.push(self.trigger);
                vars.batchMax && vars.batchMax <= batch.length && delay.progress(1);
            };
        },
        p;
    for (p in vars) {
        varsCopy[p] = (~p.indexOf("Enter") || ~p.indexOf("Leave")) ? proxyCallback(p, vars[p]) : vars[p];
    }
    gsap.utils.toArray(targets).forEach(target => {
        let config = {};
        for (p in varsCopy) {
            config[p] = varsCopy[p];
        }
        ScrollTrigger.refresh();
        config.trigger = target;
        ScrollTrigger.create(config);
    });
}