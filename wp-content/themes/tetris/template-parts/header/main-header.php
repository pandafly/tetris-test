<?php

use ThemeOptions\Helpers;
use inc\CustomFunctions;

$options = get_fields('options');
$fields = get_fields(get_the_ID());
$colorsArr = $fields['colors'] ?? '';
$changeColors = CustomFunctions::changeColor($colorsArr);
$adminStyle = CustomFunctions::styleControl($fields);
$logoType = wp_check_filetype(Helpers::get($options, 'header.logo.filename'));
$changeColorsMob = CustomFunctions::changeColor($options['mobile_menu']['colors']);
$adminStyleMob = CustomFunctions::styleControl($options['mobile_menu']);
$contactLink = get_the_permalink($options['header']['post_link'] ?? '');
$getCurrentPageLink = get_the_permalink(get_the_ID());
$kontaktPage = $contactLink == $getCurrentPageLink ? 'contact' : '';
?>
<header class="site-header <?php echo $changeColors . ' ' . $changeColorsMob; ?>"
        style="<?php echo str_replace(['"', 'style='], ['', ''], $adminStyle) . ';' . str_replace(['"', 'style='], ['', ''], $adminStyleMob); ?>">
    <div class="site-header__fixed">
        <div class="max-width-full container-fluid">
            <div class="site-header__wrapper">
                <div class="site-header__wrapper__logo">
                    <?php if (isset($logoType['ext']) && $logoType['ext'] == 'svg'): ?>
                        <a href="/" rel="home">
                            <?php echo file_get_contents(Helpers::get($options, 'header.logo.url')) ?>
                        </a>
                    <?php else: ?>
                        <?php if (function_exists('the_custom_logo') && has_custom_logo()) the_custom_logo(); ?>
                    <?php endif; ?>
                </div>
                <div class="site-header__wrapper__menu">
                    <?php $args['menu']->insertMenu(); ?>
                </div>
                <div class="site-header__wrapper__link">
                    <?php if (!empty($options['header']['post_link_text'])): ?>
                        <div class="site-header__wrapper__link__inner <?php echo $kontaktPage ?>">
                            <a href="<?php echo get_the_permalink($options['header']['post_link'] ?? '') ?>">
                                <?php echo $options['header']['post_link_text'] ?? '' ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="site-header__mobile">
                    <div class="site-header__mobile__burger"></div>
                </div>
            </div>
        </div>
        <div class="site-header__mobile__overlay">
            <div class="site-header__mobile__menu">
                <?php get_template_part('template-parts/header/mobile', 'header', $args['menu']); ?>
            </div>
        </div>
    </div>
</header>