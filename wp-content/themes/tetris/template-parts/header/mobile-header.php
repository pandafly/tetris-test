<?php
$options = get_fields('options');
if (isset($args)): ?>
    <div class="site-header__mobile__menu__wrapper">
        <?php $args->insertMenu(); ?>
        <a class="site-header__mobile__menu__link"
           href="<?php echo get_the_permalink($options['header']['post_link'] ?? '') ?>">
            <?php echo $options['header']['post_link_text'] ?? '' ?>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M2 10.5C1.17157 10.5 0.5 11.1716 0.5 12C0.5 12.8284 1.17157 13.5 2 13.5L2 10.5ZM23.0607 13.0607C23.6464 12.4749 23.6464 11.5251 23.0607 10.9393L13.5147 1.3934C12.9289 0.807611 11.9792 0.807611 11.3934 1.3934C10.8076 1.97919 10.8076 2.92893 11.3934 3.51472L19.8787 12L11.3934 20.4853C10.8076 21.0711 10.8076 22.0208 11.3934 22.6066C11.9792 23.1924 12.9289 23.1924 13.5147 22.6066L23.0607 13.0607ZM2 13.5L22 13.5V10.5L2 10.5L2 13.5Z"
                      fill="white"/>
            </svg>
        </a>
        <div class="site-header__mobile__menu__socials">
            <h3><?php echo $options['mobile_menu']['social_title'] ?? '' ?></h3>
            <?php if ($options['mobile_menu']['social_icons']): ?>
                <?php foreach ($options['mobile_menu']['social_icons'] as $item): ?>
                    <a href="<?php echo $item['link'] ?>">
                        <?php if ($item['icon']): ?>
                            <?php echo file_get_contents($item['icon']) ?>
                        <?php endif; ?>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
