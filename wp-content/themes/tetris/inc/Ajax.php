<?php

namespace Inc;

class Ajax
{
    static function init()
    {
        add_action('wp_ajax_newsLoadmore', [self::class, 'newsLoadmore']);
        add_action('wp_ajax_nopriv_newsLoadmore', [self::class, 'newsLoadmore']);
    }

    static function newsLoadmore()
    {
        $page = $_POST['page'] ?? '';
        $count = $_POST['count'] ?? '';
        $more = $_POST['more'] ?? '';
        $getPosts = get_posts(['post_type' => 'news', 'post_status' => 'publish', 'numberposts' => $count, 'offset' => $page * $count]);
        $getPostsAllOffset = get_posts(['post_type' => 'news', 'post_status' => 'publish', 'numberposts' => $count, 'offset' => ($page + 1) * $count]);
        ob_start();
        if ($getPosts) {
            foreach ($getPosts as $item) {
                get_template_part('/templates/parts/news', 'single', ['id' => $item->ID, 'more' => $more]);
            }
        }
        $html = ob_get_contents();
        ob_end_clean();
        wp_send_json(['res' => $html, 'btn' => $getPostsAllOffset ? 'true' : 'false']);
    }

}
