<?php

namespace Inc;

use ThemeOptions\Helpers;

class CustomFunctions
{
    static function init()
    {

    }

    static function getSubFields($arr)
    {
        $subFields = [];
        foreach ($arr as $item) {
            $subFields[$item] = get_sub_field($item) ?? '';
        }
        return $subFields;
    }

    static function changeColor($colors)
    {
        $arrKeys = [];
        if ($colors) {
            foreach ($colors as $index => $color) {
                if ($color != 'default' && $color != '') {
                    $arrKeys[] = $index;
                }
            }
        }
        return implode(' ', $arrKeys);
    }

    static function styleControl($fields = [])
    {
        /// Padding ///
        $customPaddingDesktop = $fields['desktop'] ?? '';
        $paddingTop = !empty($customPaddingDesktop['padding_top']) ? $customPaddingDesktop['padding_top'] . 'px' : '0';
        $paddingBottom = !empty($customPaddingDesktop['padding_bottom']) ? $customPaddingDesktop['padding_bottom'] . 'px' : '0';
        $padding = $paddingTop || $paddingBottom ? 'padding:' . $paddingTop . ' 0 ' . $paddingBottom . ';' : '';

        /// Margin ///
        $customMarginDesktop = $fields['desktop'] ?? '';
        $marginTop = !empty($customMarginDesktop['margin_top']) ? $customMarginDesktop['margin_top'] . 'px' : '0';
        $marginBottom = !empty($customMarginDesktop['margin_bottom']) ? $customMarginDesktop['margin_bottom'] . 'px' : '0';
        $margin = $marginTop || $marginBottom ? 'margin:' . $marginTop . ' 0 ' . $marginBottom . ';' : '';

        /// PaddingMobile ///
        $customPaddingMob = $fields['mobile'] ?? '';
        $paddingTopMobile = !empty($customPaddingMob['padding_top']) ? $customPaddingMob['padding_top'] . 'px' : '0';
        $paddingBottomMobile = !empty($customPaddingMob['padding_bottom']) ? $customPaddingMob['padding_bottom'] . 'px' : '0';
        $paddingMobile = $paddingTopMobile || $paddingBottomMobile ? '--padding-mob:' . $paddingTopMobile . ' 0 ' . $paddingBottomMobile . ';' : '';

        /// MarginMobile ///
        $customMarginMob = $fields['mobile'] ?? '';
        $marginTopMobile = !empty($customMarginMob['margin_top']) ? $customMarginMob['margin_top'] . 'px' : '0';
        $marginBottomMobile = !empty($customMarginMob['margin_bottom']) ? $customMarginMob['margin_bottom'] . 'px' : '0';
        $marginMobile = $marginTopMobile || $marginBottomMobile ? '--margin-mob:' . $marginTopMobile . ' 0 ' . $marginBottomMobile . ';' : '';

        /// change colors ///
        $colors = get_sub_field('colors') ?: $fields['colors'] ?? '';
        $colorsStr = '';
        if ($colors) {
            foreach ($colors as $key => $color) {
                if (isset($color) && $color != 'default') {
                    $newKey = str_replace('_', '-', $key);
                    $colorsStr .= '--' . $newKey . ':rgb(' . $color . ');';
                }
            }
        }
        $bgAdd = 'style="' . $colorsStr . $paddingMobile . $marginMobile . $padding . $margin . '"';
        return $bgAdd;
    }

    static function colorsControl($fields = [])
    {
        /// change colors ///
        $colors = $fields ?? '';
        $colorsStr = '';
        if ($colors) {
            foreach ($colors as $key => $color) {
                if (isset($color) && $color != 'default') {
                    $newKey = str_replace('_', '-', $key);
                    $colorsStr .= '--' . $newKey . ':rgb(' . $color . ');';
                }
            }
        }
        $bgAdd = 'style="' . $colorsStr . '"';
        return $bgAdd;
    }
}
