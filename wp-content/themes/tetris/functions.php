<?php

use inc\Ajax;
use inc\CustomFunctions;
use ThemeOptions\PostType;
use ThemeOptions\EnqueueScripts;
use ThemeOptions\Options;
use ThemeOptions\AcfBlocksEnable;
use ThemeOptions\RegisterMenus;
use ThemeOptions\PostTaxonomy;


class Theme
{

    static function init()
    {
        self::autoload();
        self::registerThemeSettings();
        self::initThemeMethods();
        Ajax::init();
        CustomFunctions::init();
    }

    static function autoload()
    {
        require_once(realpath(get_template_directory()) . '/inc/loader.php');
    }

    static function registerThemeSettings()
    {
        new EnqueueScripts(self::themeStyles(), self::themeScripts());
        new Options();
        new AcfBlocksEnable(self::acfBlocks());
    }

    static function initThemeMethods()
    {
        self::addCustomPostType();
        self::addCustomTaxonomy();
        self::registerMenu();
        self::addImageSizes();
        self::customThemeHooks();
    }

    static function customThemeHooks()
    {
        add_action('admin_enqueue_scripts', [self::class, 'load_admin_styles']);
        add_filter('show_admin_bar', '__return_false');
        add_action('admin_head', [self::class, 'remove_content_editor']);
        add_filter('acf/load_field/name=header_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=title_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=text_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=bg_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=tab_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=btn_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=svg_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=hover_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=caption_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=link_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=line_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=bg_above_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=txt_above_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=bg_under_color_radio', [self::class, 'сolorRadio']);
        add_filter('acf/load_field/name=txt_under_color_radio', [self::class, 'сolorRadio']);
    }

    static function сolorRadio($field)
    {
        $BgColorRadioArr = [
            '26,26,26'      => '#1A1A1A',
            '255, 255, 255' => '#FFFFFF',
            '234,86,0'      => '#EA5600',
            '42,86,80'      => '#2A5650',
            '233,221,203'   => '#E9DDCB',
            '241,240,235'   => '#F1F0EB',
            '0,0,0'         => '#000000',
            '202,170,146'   => '#CAAA92',
            '66,0,6'        => '#420006',
        ];
        $field['choices']['default'] = '<div class="admin_circle"><p>Default</p></div>';
        foreach ($BgColorRadioArr as $key => $item) {
            $field['choices'][$key] = '<div class="admin_circle"></span><p>' . $item . '</p><span style="background-color: ' . $item . '"' . '></div>';
        }

        return $field;
    }

    static function load_admin_styles()
    {
        wp_enqueue_style('admin_css', get_template_directory_uri() . '/dest/css/style-admin.css', false, time());
    }

    static function remove_content_editor()
    {
        remove_post_type_support('page', 'editor');
        remove_post_type_support('news', 'editor');
        remove_post_type_support('team', 'editor');
        remove_post_type_support('referencer', 'editor');
        remove_post_type_support('quotes', 'editor');
    }

    static function acfBlocks(): array
    {
        $path = get_template_directory_uri() . '/dest/images/gutenberg/';
        return [
            'acf_block_hero' => ['title' => 'Hero Module', 'preview' => $path . 'acf_block_hero_image.jpg'],
        ];
    }


    static function themeStyles(): array
    {
        return [
            'app-style'     => get_template_directory_uri() . '/dest/css/app.css',
            'swiper-bundle' => get_template_directory_uri() . '/dest/css/swiper-bundle.min.css',
            'style-admin'   => get_template_directory_uri() . '/dest/css/style-admin.css',
        ];
    }

    static function themeScripts(): array
    {
        return [
            'app-script-imagesloaded-pkgd-min' =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/imagesloaded.pkgd.min.js',
                    'connection' => ['jquery'],
                ],
            'app-script-owl-carousel-min'      =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/swiper-bundle.min.js',
                    'connection' => ['jquery'],
                ],
            'slider-class'                     =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/inc/Slider.js',
                    'connection' => ['app-script-owl-carousel-min', 'app-script-imagesloaded-pkgd-min'],
                ],
            'animation-class'                  =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/inc/Animation.js',
                    'connection' => ['gsap', 'gsap-scroll-trigger'],
                ],
            'gsap'                             =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/gsap.min.js',
                    'connection' => ['jquery'],
                ],
            'lottie'                           =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/lottie.min.js',
                    'connection' => ['jquery'],
                ],
            'gsap-scroll-trigger'              =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/ScrollTrigger.min.js',
                    'connection' => ['jquery'],
                ],
            'smooth-script'                    =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/smoot-scrollbar.js',
                    'connection' => ['jquery'],
                ],
            'app-jquery-script'                =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/app_jquery.js',
                    'connection' => ['slider-class'],
                ],
            'app-animation-script'             =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/app_animation.js',
                    'connection' => ['animation-class'],
                ],
            'app-script'                       =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/app.js',
                    'connection' => ['jquery'],
                ],
            'loc-script'                       =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/libraries/locomotive-scroll.min.js',
                    'connection' => ['jquery'],
                ],
            'map-script'                       =>
                [
                    'url'        => get_template_directory_uri() . '/assets/js/map.js',
                    'connection' => ['jquery'],
                ],
//            'google-script'                    =>
//                [
//                    'url'        => 'https://maps.googleapis.com/maps/api/js?key=' . constant('GOOGLE_MAP') . '&callback=initMap',
//                    'connection' => [],
//                    time(),
//                    true,
//                ],
        ];
    }


    static function addCustomPostType()
    {
        new PostType('News', basename(__DIR__), ['supports' => ['title', 'thumbnail', 'editor', 'author', 'excerpt']]);
        new PostType('Team', basename(__DIR__));
        new PostType('Referencer', basename(__DIR__));
        new PostType('Quotes', basename(__DIR__));
    }


    static function addCustomTaxonomy()
    {
//        new PostTaxonomy('Taxonomy name', 'post-type-name', basename(__DIR__));
    }


    static function registerMenu()
    {
        $menuArray = ['MainHeader'];
        $registerAllMenuItems = new RegisterMenus('', basename(__DIR__), $menuArray);
        $registerAllMenuItems->addAction();
    }

    static function addImageSizes()
    {
        add_image_size('large', 1900, 800);
        add_image_size('medium', 800, 600);
        add_image_size('small', 400, 250);
    }

}


Theme::init();
