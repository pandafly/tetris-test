<?php
/**
 * Created by PhpStorm.
 * User: bvn-564
 * Date: 6/18/21
 * Time: 4:50 PM
 */

namespace ThemeOptions;


class Helpers
{
    static function dd()
    {
        echo '<pre>';
        array_map(function ($x) {
            var_dump($x);
        }, func_get_args());
        die;
    }

    static function get_template_part_var($template, $data = [])
    {
        extract($data);
        require locate_template($template . '.php');
    }

    static function get($value, $keysDot, $default = null)
    {
        $keys = explode('.', $keysDot);

        foreach ($keys as $key) {
            $value = is_array($value) && array_key_exists($key, $value) ? $value[$key] : $default;
            if ($value === $default) break;
        }

        return $value;
    }

    static function importImageForGutenbergBlock($src)
    {
        echo '<img src="' . $src . '">';
    }

    static function getField($field, string $class = '', string $tag = 'div'): string
    {
        if (empty($field)) {
            return '';
        }

        if ($class) {
            $class = sprintf(' class="%s"', $class);
        }

        return sprintf('<%1$s%2$s>%3$s</%1$s>', $tag, $class, $field);
    }

    static function getLink($field, string $class = 'link-underline'): string
    {
        if (empty($field)) {
            return '';
        }

        if (empty($title = $field['title'])) {
            return '';
        }

        $url = !empty($field['url']) ? $field['url'] : '/';
        $target = !empty($field['target']) ? 'target="' . $field['target'] . '"' : '';
        $class = !empty($class) ? 'class="' . $class . '"' : '';

        return '<a href="' . $url . '" ' . $class . $target . '>' . $title . '</a>';
    }

    static function getImg($id, string $size = 'thumbnail', string $format = '', string $class = ''): string
    {
        if (empty($id)) {
            return '';
        }

        $image = wp_get_attachment_image($id, $size, '', ['class' => $class]);

        if (!$image) {
            $defaultImageId = get_field('default_image', 'options');
            return wp_get_attachment_image($defaultImageId, $size) ?: '';
        }

        if ($format === 'url') {
            return wp_get_attachment_image_url($id, $size);
        }

        return $image;
    }

    static function getPosts(array $args = []): array
    {
        $args =  array_merge([
            'post_type'   => 'post',
            'numberposts' => 3,
            'orderby'     => 'date',
            'order'       => 'desc',
        ], $args);

        return get_posts($args);
    }

    static function textLimit(string $text, int $limit = 100): string
    {
        $plainText = trim(strip_tags($text));
        $clearText = str_replace('&nbsp;', '', $plainText);
        return strlen($clearText) > $limit ? mb_substr($clearText, 0, $limit, 'utf-8') . '...' : $clearText;
    }

    static function getThumbnail(int $pageId = null, string $size = 'thumbnail', string $format = 'array'): string
    {
        if (!has_post_thumbnail($pageId)) {
            $defaultImageId = get_field('default_image', 'options');

            if ($format === 'url') {
                return wp_get_attachment_image_url($defaultImageId, $size) ?: '';
            }

            return wp_get_attachment_image($defaultImageId, $size) ?: '';
        }

        if ($format === 'url') {
            return get_the_post_thumbnail_url($pageId, $size);
        }

        return get_the_post_thumbnail($pageId, $size);
    }
}
